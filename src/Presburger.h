/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef PRESBURGER_H
#define PRESBURGER_H

extern "C" {
#include "dfa.h"
}
using namespace std;
//class Affine;

// DFA *DFAplus;  /* p_2 = p_0 + p_1 */
// DFA *DFAeq;    /* p_0 = p_1       */
// DFA *DFA2times;/* p_0 = 2.p_1     */
// DFA *DFAless;  /* p_0 < p_1 */
// DFA *DFAmore;  /* p_0 > p_1 */
// DFA *DFA1;     /* no condition */
// DFA *DFA0;     /* emptyset */



// On se donne un entier $n\geq0$ et deux liste $pos[i]$ et $val[i]$ 
//   On construit le DFA representant $\sum_{i=0}^{n-1}val[i].p_{pos[i]}=c$
DFA *dfaPresburgerEquation(const int *val, const int *pos, const int c, const int n);

// Initialisation du package presburger   
void fast_init();

// fermeture du package presburger et reallocation de la memoire   
void fast_close();

#endif
