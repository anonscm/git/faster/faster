/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "PickUP.h"

PickUP::PickUP(int n)
{
  for(int i=0;i<n;i++)
    vec.push_back(i);
}
int PickUP::randomGet() 
{
  int p=int((double(rand())/RAND_MAX)*((int)size()));
  int x=vec[p];
  vec[p]=vec[size()-1];
  vec.pop_back();
  return x;
}

int PickUP::size() const
{
  return (int)vec.size();
}

void PickUP::empty()
{
  vec=VectorInt();
}

void PickUP::remove(int x)
{
  int p=size();
  for(int pos=0;pos<size();pos++)
    if (vec[pos]==x)
      p=pos;
  vec[p]=vec[size()-1];
  vec.pop_back();  
}

ostream & operator<<(ostream & os, const PickUP &pUP)
{
  for(int i=0;i<(int)pUP.vec.size();i++)
    os<<pUP.vec[i]<<", ";
  os<<endl;
  return os;
}
