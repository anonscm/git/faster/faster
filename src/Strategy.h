

#ifndef STRATEGY_H
#define STRATEGY_H


using namespace std;
#include "heuristic.h"
#include "arbresyntaxique.h"


extern LNDD regionToLNDD (region reg, map<string, LNDD> memory, map<string, LinearSystem> memoryTrans,  const Order & o,  LinearSystem s, map<string,int>  stateval);

extern bool boolFormToBool (boolForm bf, map<string, bool> memory, map<string, LNDD> memReg, map<string, LinearSystem> memTrans, const Order & o,  LinearSystem s, map<string,int>  stateval) ;

extern LinearSystem TransitionsToLinearSystem (Transitions trans, map<string, LinearSystem> memory, const Order & o, LinearSystem s);


typedef map<string, LNDD> map_string_LNDD;


class Strategy 
{
 public:

  
  LinearSystem LS;  
  map<string,int> stateval;
  instructionlist instructions;
  map<string, bool> varbool;
  map<string, LinearSystem> vartrans;
  map_string_LNDD varreg;
  
  Strategy(instructionlist il, const LinearSystem & L, map<string,int> sv);

  void decl(declaration i);
  
  void ifthenelse(ifThenElse ite);
  
  void proc (procedure proc);

  void start();
  
};


#endif
