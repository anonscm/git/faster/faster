/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "arbsynToFast.h"

// c'est juste pour tester
/*bool FORWARD = true;
bool BACKWARD= false;
LNDD heuristique(const LinearSystem &Ls,
                 const LNDD &startStates,
                 bool direction,
                 int k=1,
                 bool onetimeuse=false,
		 bool removeunused=false,bool saturate=false)
{
  return LNDD::nothing(Ls.order);
}
*/




// remarque : le linearsumTree est suppose bien ecrit  (pas de a*b pa exemple)

// fonction pour enlever le ' a la fin d'un string
string unprimeString (string s) 
{
  string res = s;
  res.erase(res.begin()+res.size()-1);
  return res;
}

LinearSum linearsumTreeToLinearSum (linearsumTree t) 
{
  LinearSum sum;
  int* i = (int*)malloc(sizeof(int));
  

   if (strcmp(t.val,"*")==0) {sum = atoi((t.left)->val) * linearsumTreeToLinearSum(*(t.right));}  // on sait que left est un nombre !!
   else {if (strcmp(t.val,"+")==0) {sum = linearsumTreeToLinearSum(*(t.left))+linearsumTreeToLinearSum(*(t.right));}
         else {if (strcmp(t.val,"-")==0) {sum = linearsumTreeToLinearSum(*(t.left))-linearsumTreeToLinearSum(*(t.right));}
	   else {if (isinteger(t.val,i)) {sum = LinearSum(*i);} // on est sur une feuille
	             else {sum = LinearSum(t.val);}
		    }	
	       }
	}
  free(i);
  return sum;
}

LNDD pbgTermToLNDD (pbgTerm pbgt, const Order & order)
{
  
  return  LNDD(linearsumTreeToLinearSum(pbgt.lsum), pbgt.op, order);  
}

LNDD pbgFormToLNDD (pbgForm form,  const Order & order,  map<string,int>  stateval)
{
  Order ord2=order;
  
  LNDD lndd = LNDD::nothing(order);  
  if (form.type == PBGTERM) {lndd = pbgTermToLNDD(form.val.term,order);}
  else if (form.type == PBGOP) 
       { if  (strcmp(form.val.op,"AND")==0) {lndd = pbgFormToLNDD(*(form.left),order,stateval) & pbgFormToLNDD(*(form.right),order,stateval);} 
         else if  (strcmp(form.val.op,"OR")==0) {lndd = pbgFormToLNDD(*(form.left),order,stateval) | pbgFormToLNDD(*(form.right),order,stateval);} 
         else if  (strcmp(form.val.op,"NO")==0) {lndd = !pbgFormToLNDD(*(form.right),order,stateval);} 
         else if  (strcmp(form.val.op,"true")==0) {lndd = LNDD::everything(order);} 
         else if  (strcmp(form.val.op,"false")==0) {lndd = LNDD::nothing(order);} 
         else cerr << "ERROR : unknown operator !!!!!!!!!"<<endl;
       }
  else if (form.type == PBGQUANT)  {ord2.push_back(form.val.quant.ident); lndd = LNDD::exist (form.val.quant.ident, pbgFormToLNDD(*(form.right),ord2,stateval) );}  
  else if (form.type==PBGSTATE)  {lndd =  LNDD(LinearSum("state") - stateval[form.val.state],"==0",order);}  
    else cerr << "ERROR : unknown pbgForm type !!!!!!!!!"<<endl;
  return lndd;
}

SquareMatrix actionToMatrix (action act,  const Order & order, SquareMatrix s)
{
  LinearSum l = linearsumTreeToLinearSum (act.affect);

  int m = order.position(unprimeString(act.ident))+1; 
  //int n =1;  

  SquareMatrix sqm = s;
  sqm(m,m)=0;  // on suppose que de base p est identite donc p.sqp[i][i] = 1
  
  for(LinearSum::MapIntInt::const_iterator i=l.iimap.begin();i!=l.iimap.end();i++){    
    sqm(m,order.position(i->first)+1) = i->second; 
  }
  return sqm;  
}

Vector actionToVector (action act,  const Order & order, Vector vct)
{
  LinearSum l = linearsumTreeToLinearSum (act.affect);
  int m = order.position(unprimeString(act.ident))+1;  

  Vector v = vct;
  v(m,1) = l.getConstante();  // on suppose v[i] = 0 au depart 
  return v;
}

SquareMatrix actionListToMatrix (actionlist actlist,  const Order & order)
{
  if (actlist.next != NULL) {return actionToMatrix(actlist.val, order, actionListToMatrix (*(actlist.next), order));}
    else  {return actionToMatrix(actlist.val, order, SquareMatrix::identity(order));} 
}

Vector actionListToVector (actionlist actlist,  const Order & order)
{
    if (actlist.next != NULL) {return actionToVector(actlist.val, order, actionListToVector (*(actlist.next), order));}
    else  {return actionToVector(actlist.val, order, Vector::zero(order));} 
}
    
Affine transToAffine (transition trans, const Order & order, map<string,int>  stateval)
{  
  LNDD guard = pbgFormToLNDD(trans.guard,order,stateval); // rajout de l'etat initial  dans la garde
  guard = guard &  LNDD(LinearSum("state") - (stateval[trans.from]),"==0",order);
  

  char* s = intTostring (stateval[trans.to]);
  action ac =  CreateAction ("state'", stringToLstree(s)) ; //rajout de l'action sur l'etat 
  free (s);
  actionlist acl = trans.action + ac;
  

  SquareMatrix M = actionListToMatrix (acl, order);  
  Vector v = actionListToVector (acl, order);  


    Affine aff (M, v, guard, order, trans.name);
    return aff;   
}


LinearSystem transListToLSys  (transitionlist tl, const Order & order, map<string,int>  stateval, string name)
{  
   LinearSystem L(order, name);
   bool end = false;
   transitionlist tlist = tl;
   while (!end) {
     L.insert(transToAffine(tlist.val,order,stateval));
     if (tlist.next==NULL) {end=true;}     
     else tlist= *(tlist.next);
   }   
   return L;    
}

LinearSystem modelToLSys (model mod)
{
  Order order;
  map<string, int> stateval;
  
  stringlist sl = mod.vars;
  bool end = false;  
   while (!end) {
     order.push_back(sl.val);
     //prime[sl.val]= string(sl.val)+"'";     
     if (sl.next==NULL) end=true;
     else sl= *(sl.next);
   }     
   order.push_back("state");
   //prime["state"]= "state'";     
 
  sl = mod.states;
  end = false; 
   while (!end) {
     stateval[sl.val]= ModStateToInt (mod, sl.val); //cout << sl.val <<" "<< stateval[sl.val]<<endl;
     if (sl.next==NULL) end=true;
     else sl= *(sl.next);
   }        

  return transListToLSys(mod.trans, order, stateval, mod.name);  
}

// **************************

LNDD regionTermToLNDD (regionTerm regterm, map<string, LNDD> memory, const Order & o, map<string,int>  stateval)
{
  if (regterm.type==1) {
    return (*memory.find(regterm.val.id)).second;
  }  
  else {   
    return pbgFormToLNDD(regterm.val.form, o, stateval);
  }
}



LNDD regionToLNDD (region reg, map<string, LNDD> memory,  map<string, LinearSystem> memoryTrans,   const Order & o,  LinearSystem s, map<string,int>  stateval)
{
  Order order=o;
  LNDD lndd = LNDD::nothing(order);  
  //LNDD inv = LNDD::everything(order);    

  if (reg.type==1) {
    lndd = regionTermToLNDD(reg.val.term,  memory, o, stateval); 
  }
  else if (reg.type==2) {    
           if  (strcmp(reg.val.op,"AND")==0) {
	     lndd = regionToLNDD(*(reg.left),memory,memoryTrans,order,s,stateval) & regionToLNDD(*(reg.right),memory,memoryTrans,order,s,stateval);} 
	   else if  (strcmp(reg.val.op,"OR")==0) {
	     lndd = regionToLNDD(*(reg.left),memory,memoryTrans,order,s,stateval) | regionToLNDD(*(reg.right),memory,memoryTrans,order,s,stateval);} 
	   else if  (strcmp(reg.val.op,"NO")==0) {
	     lndd = !regionToLNDD(*(reg.right),memory,memoryTrans,order,s,stateval);} 
	   else cout<<"error in regionToLNDD"<<endl;  	   
  }
  else if (reg.type==3) {  
    LinearSystem L= TransitionsToLinearSystem(reg.val.fun.trans,memoryTrans,o,s);    
    LNDD S0 = regionToLNDD(*(reg.right),memory,memoryTrans,order,s,stateval); 
    int k = reg.val.fun.k;
    if  (strcmp(reg.val.fun.fun,"pre*")==0) {
      lndd =heuristique(L,S0,BACKWARD,k);}    
    else if  (strcmp(reg.val.fun.fun,"post*")==0) {
      lndd =lndd =heuristique(L,S0,FORWARD,k);}
    else cout<<"error in regionToLNDD"<<endl;  
  }
  else if (reg.type==4) { 
    lndd = LNDD::exist(reg.val.op, regionToLNDD(*(reg.right),memory,memoryTrans,order,s,stateval));    
  }
  else cout<<"error in regionToLNDD"<<endl;  


    
  return lndd; 
}

// **************************


bool  boolTermToBool (boolTerm bt, map<string, bool> memory, map<string, LNDD> memReg, map<string, LinearSystem> memTrans, const Order & o,  LinearSystem s, map<string,int>  stateval)
{  
  bool res;
  if (bt.type==1) { 
    if  (strcmp(bt.id,"True")==0) res=true;
    else     if  (strcmp(bt.id,"False")==0) res=false;
    else     res= (*memory.find(bt.id)).second; 
  }  
  else if (bt.type==3) {
    if  (strcmp(bt.id,"eqset")==0) {
      LNDD L1= regionToLNDD(*(bt.regleft),memReg,memTrans,o,s,stateval);
      LNDD L2= regionToLNDD(*(bt.regright),memReg,memTrans,o,s,stateval);
      res= (L1==L2);
    }
    else if  (strcmp(bt.id,"subset")==0) {
      LNDD L1= regionToLNDD(*(bt.regleft),memReg,memTrans,o,s,stateval);
      LNDD L2= regionToLNDD(*(bt.regright),memReg,memTrans,o,s,stateval);
      res= (L1<=L2);      
    }
    else  cout<<"error in BoolTermToLNDD"<<endl;
  }
  else if (bt.type==2) {
    if (strcmp(bt.id,"isempty")==0) {
      LNDD L= regionToLNDD(*(bt.regright),memReg,memTrans,o,s,stateval);
      res = L.isEmpty();
    }	 
    else  cout<<"error in BoolTermToLNDD"<<endl;
  }  
  else cout<<"error in BoolTermToLNDD"<<endl;  
  return res;
}


bool boolFormToBool (boolForm bf, map<string, bool> memory, map<string, LNDD> memReg, map<string, LinearSystem> memTrans, const Order & o,  LinearSystem s, map<string,int>  stateval)
{ // pas fini

  bool res;
  if (bf.type==1) res= boolTermToBool(bf.val.term,memory,memReg,memTrans, o,s,stateval);  
  else if (bf.type==2) {    
           if  (strcmp(bf.val.op,"AND")==0) {res = boolFormToBool(*(bf.left),memory,memReg,memTrans, o,s,stateval ) & boolFormToBool(*(bf.right),memory,memReg,memTrans, o,s,stateval);} 
	   else if  (strcmp(bf.val.op,"OR")==0) {res = boolFormToBool(*(bf.left),memory,memReg,memTrans, o,s,stateval) | boolFormToBool(*(bf.right),memory,memReg,memTrans, o,s,stateval);}
	   else if  (strcmp(bf.val.op,"EQU")==0) {res = (boolFormToBool(*(bf.left),memory,memReg,memTrans, o,s,stateval) == boolFormToBool(*(bf.right),memory,memReg,memTrans, o,s,stateval));} 	   
	   else if  (strcmp(bf.val.op,"NO")==0) {res = !boolFormToBool(*(bf.right),memory,memReg,memTrans, o,s,stateval);} 
	   else cout<<"error in boolFormToBool"<<endl;  	   
  }
  else cout<<"error in boolFormToLNDD"<<endl;      
  return res;     
}




// **************************


Affine LinearSystemFind (LinearSystem L, string s)
{
  for(LinearSystem::const_iterator j=L.begin();j!=L.end();j++) {
    if (j->name == s) return *j;    
  }
  cout << "error in LinearSystemFind"<<endl; 
  return L[0];
}

LinearSystem StringlistToLinearSystem(stringlist sl, const Order & o, LinearSystem LS, string name )
{
  LinearSystem L(o,name);  
  stringlist slist =sl;  
  bool end=false;
  
   while (!end) {     
     L.insert(LinearSystemFind(LS,slist.val));
     if (slist.next==NULL) {end=true;}     
     else slist= *(slist.next);
   }   
   return L;
}

// Modification legere du code de S.Bardin par J.Leroux pour eviter l'affichage de id.t au lieu de t
Affine TransitionBasicToAffine(LinearSystem L, stringlist sl)
{
  stringlist worklist = sl;
  bool end=false;
  Affine aff=LinearSystemFind(L,worklist.val);
  if (worklist.next==NULL) {end=true;}     
  else worklist= *(worklist.next);
  while (!end) {     
    aff = compose(LinearSystemFind(L,worklist.val),aff);  // calcul 
    if (worklist.next==NULL) {end=true;}     
    else worklist= *(worklist.next);
  }    

  return aff;   
}


LinearSystem TransitionsBasiqueListToLinearSystem(TransitionsBasiqueList sl, const Order & o, LinearSystem LS, string name )
{
  LinearSystem L(o,name);  
  TransitionsBasiqueList tbl =sl;  
  bool end=false;
  
   while (!end) {     
     L.insert(TransitionBasicToAffine(LS,tbl.val));
     if (tbl.next==NULL) {end=true;}     
     else tbl= *(tbl.next);
   }   
   return L;
}


/* LinearSystem TransitionsToLinearSystem (Transitions trans, map<string, LinearSystem> memory, const Order & o,  LinearSystem s) */
/* {    */
/*   if (trans.type==1) {return (*(memory.find(trans.val.id))).second;}   */
/*   else {return StringlistToLinearSystem(trans.val.tl, o, s, "");}   */
/* } */

LinearSystem TransitionsToLinearSystem (Transitions trans, map<string, LinearSystem> memory, const Order & o,  LinearSystem s) 
{   
  if (trans.type==1) {return (*(memory.find(trans.val.id))).second;}  
  else {return TransitionsBasiqueListToLinearSystem(trans.val.tl, o, s, "");}  
}

// **************************

Strategy modelStrategyToStrategy (modelStrategy modStra)
{
  
  model mod = modStra.mod;
  instructionlist il = modStra.strateg.val;

  Order order;
  map<string, int> stateval;
  
  stringlist sl = mod.vars;
  bool end = false;  
   while (!end) {
     order.push_back(sl.val);
     //prime[sl.val]= string(sl.val)+"'";     
     if (sl.next==NULL) end=true;
     else sl= *(sl.next);
   }     
   order.push_back("state");
   //prime["state"]= "state'";     
 
  sl = mod.states;
  end = false; 
  int nstates=-1;  
   while (!end) {
     stateval[sl.val]= ModStateToInt (mod, sl.val);//cout << sl.val <<" "<< stateval[sl.val]<<endl;
     nstates++;     
     if (sl.next==NULL) end=true;
     else sl= *(sl.next);
   }        

  LinearSystem res1 = transListToLSys(mod.trans, order, stateval, mod.name);    

  return Strategy(il, res1, stateval);
  
}
