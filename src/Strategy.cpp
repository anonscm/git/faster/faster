/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "Strategy.h" 



instructionlist* append(instructionlist* head, instructionlist* tail)
{
  instructionlist* res=head;
  if (head==NULL) return tail;
  else {    
  instructionlist* current=head;
  while(current->next!=NULL) current=current->next;
  current->next=tail;  
  return res; 
  }
  
}


Strategy::Strategy(instructionlist il, const LinearSystem &  L,  map<string,int> sv ):LS(L),stateval(sv),instructions(il)
{
  varreg = map<string,LNDD>();

}


void Strategy::decl(declaration i)
{
  if (i.type==1) {
    varbool[i.id]=boolFormToBool(i.val.boolF, varbool,varreg,vartrans, LS.order, LS, stateval);
 }
  else if (i.type==2) { 
    vartrans[i.id]=TransitionsToLinearSystem(i.val.trans,vartrans, LS.order, LS);    
}
  else if (i.type==3) {
    varreg[i.id]=regionToLNDD(i.val.reg,varreg,vartrans,LS.order, LS,stateval);
    
 }
  else cout <<"error in Strategy::declaration(declaration)"<<endl; 
}

  
void Strategy::ifthenelse(ifThenElse ite)
{
  bool cond = 
    boolFormToBool(ite.cond, varbool,varreg,vartrans, LS.order, LS, stateval);
  if (cond) {
    instructionlist* newlist=append(ite.If, instructions.next);
    instructions.next=newlist;
  }
  else {
    instructionlist* newlist=append(ite.Else, instructions.next);
    instructions.next=newlist;
  }
}

  
void Strategy::proc (procedure proc)
{
  if (strcmp (proc.name, "setMaxState") == 0) {
    MAX_STATES = atoi (proc.argv[0]);
  } else if (strcmp (proc.name, "setMaxAcc") == 0) {
    MAX_ACCELERATIONS = atoi (proc.argv[0]);
  } else if (strcmp (proc.name, "setAcc") == 0) {
    if (strcmp(proc.argv[0], "\"standard\"") == 0) 
      ACCELERATION = 0; 
    else if (strcmp(proc.argv[0], "\"convex\"") == 0) 
      ACCELERATION = 1;
    else if (strcmp(proc.argv[0], "\"positive\"") == 0)
      ACCELERATION = 2; 
    else if (strcmp(proc.argv[0], "\"post\"") == 0) 
      ACCELERATION = 3;    
    else 
      cout << "unknown acceleration algorithm " << proc.argv[0] << endl;
  } else if (strcmp (proc.name, "setHeuristic" ) == 0) {
    if (strcmp (proc.argv[0], "\"best\"") == 0) 
      HEURISTIC = 0; 
    else if (strcmp (proc.argv[0], "\"depth\"") == 0)
      HEURISTIC = 1;    
    else 
      cout << "unknown heuristic" << endl;        
  } else if (strcmp (proc.name, "setReductionOn") == 0) {
    if (strcmp (proc.argv[0], "\"conjugacy\"") == 0) 
      red_conj = true; 
    else if (strcmp (proc.argv[0], "\"commutation\"") == 0)
      red_commutation = true;
    else if (strcmp (proc.argv[0], "\"union\"") == 0)
      red_union = true;     
    else 
      cout << "unknown reduction" << endl;
  } else if (strcmp(proc.name, "setReductionOff" ) == 0) { 
    if (strcmp(proc.argv[0], "\"conjugacy\"") == 0) 
      red_conj = false; 
    else if (strcmp (proc.argv[0], "\"commutation\"") == 0)
      red_commutation = false;
    else if (strcmp (proc.argv[0], "\"union\"") == 0) 
      red_union = false;     
    else 
      cout << "unknown reduction" << endl;      
  } else if (strcmp (proc.name, "print") == 0) {
    FILE *output;
    map_string_LNDD::iterator i = varreg.find (proc.argv[0]);

    if (proc.argc == 2) {
      output = fopen (proc.argv[1], "w");
      if (output == NULL) {
	cerr << "can't open file '" << proc.argv[1] << endl;
      }
    } else {
      output = stdout;
    }

    if (output != NULL) {
      if (i != varreg.end ()) {
	varreg[proc.argv[0]].displayAllElements (output);
      } else {
	fprintf (output, "%s", proc.argv[0]);
      }
      fflush (output);
      if (proc.argc == 2) {
	fclose (output);
      }
    } 
  } else
    cout << "error in Strategy::procedure(procedure)" << endl;  
}


void Strategy::start()
{
  bool end=false;
  instruction i;
  
  while (!end){
    i=instructions.i;
    if (i.type == 1) {
      //cout<<"decl"<<endl;
      decl(i.val.decl);
    }
    else if (i.type == 2) {
      //cout<<"proc"<<endl;
      proc(i.val.proc);
    }
    else if (i.type == 3) {
      //cout<<"ifthenelse"<<endl;
      ifthenelse(i.val.ite);
    }
    else cout<<"error in Strategy::start()"<<endl;

    if (instructions.next!=NULL) instructions=*(instructions.next);
    else end=true;
  }
}

  



