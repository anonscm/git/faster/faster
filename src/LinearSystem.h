/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef LINEARSYSTEM_H
#define LINEARSYSTEM_H
#include "Affine.h" 

extern int VERBOSE;

class LinearSystem: public vector<Affine>
{
 public:
  Order order;
  string name;

  LinearSystem(const Order &order, const string &name="");
  LinearSystem();
  

  LinearSystem removeGuard(void) const;
  LinearSystem simplify(void) const;
  
  void insert(const Affine &f);
  bool fixPoint(const LNDD & set, const bool direction) const; // teste si a est un point fixe pour le linear system
};

LinearSystem compose(const LinearSystem & L1,const LinearSystem & L2);

#endif
