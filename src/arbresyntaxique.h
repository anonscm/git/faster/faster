/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef PARS_H
#define PARS_H

using namespace std;

#include <cstdio>
#include <iostream>  
//#include <malloc.h>
#include <cstring>
#include <cstdlib>

// pour les formules de Presburger
#define PBGTERM 1
#define PBGOP 2
#define PBGQUANT 3
#define PBGSTATE 4

// pour les bools termes
//#define VARiable 1
#define BINFUN 3
#define UNFUN 2

// pour les booleens
#define BOOLTERM 1
#define BOOLOP 2

// pour les Transitions (strategy)
#define ID 1
#define STRINGLIST 2

// regionTerm
#define ID 1
#define PBGFORM 2

// region
#define TERM 1
#define OP 2
#define FUNCTION 3
#define QUANTIFIER 4

// declaration
#define BOOL 1
#define TRANS 2
//#define REGION 3 

// pour les instructions
#define DECLARATION 1
#define PROCEDURE 2
#define IFTHENELSE 3

bool isinteger (const char* s, int* b);

char* intTostring (int i);


/* deux structures presburgerTree et linearsumTree pour representer les equations de Presburger et des sommes lineaires */
/* et une structure pour representer un modele */

// **************** linearsumTree ******************

typedef struct _linearsumTree   // exemple : 3x + 6y
{
  char* val;
  struct _linearsumTree*  left;
  struct _linearsumTree* right;
} linearsumTree;

linearsumTree stringToLstree (const char* s);

linearsumTree operator+(const linearsumTree &t1, const linearsumTree &t2);

linearsumTree operator-(const linearsumTree &t1, const linearsumTree &t2);

linearsumTree operator*(const char* s, const linearsumTree &t2); // le produit scalaire, s est un entier !!
linearsumTree operator*(int i, const linearsumTree &t2); 

linearsumTree operator-(const linearsumTree &t1);

ostream & operator <<(ostream & os, const linearsumTree & t);


// ***************** presburgerTerm ***************

typedef struct  _pbgTerm   // valeur d'un presburgerTerm (linearsum et operateur >=0 ou ==0) exemple : 3x + 6y - z >= 0
{
  linearsumTree lsum;
  char* op;  // op a les valeurs "==0" et ">=0"
} pbgTerm;

pbgTerm pbgTermCreate (linearsumTree lt, const char* opt); // faire fonctio d'erreur si opt a pas bonnes valeurs

ostream & operator <<(ostream & os, const pbgTerm & pbgt);


// ***************** presburgerFormula ************

typedef union _formVal 
{
  pbgTerm term;
  char* op;
  struct pbgQuant {char* q; char* ident;} quant;  
  char* state;  
} formVal;


typedef struct  _pbgForm   // formule de Presburger complete
{
  int type;
  formVal val;
  struct _pbgForm* left;
  struct _pbgForm* right;  
} pbgForm;

pbgForm termToForm (pbgTerm pbgt);

pbgForm pbgFormState (char* s);   // assertion sur l'etat de controle courant

pbgForm operator&&(const pbgForm & form1, const pbgForm & form2);

pbgForm operator||(const pbgForm & form1, const pbgForm & form2);

pbgForm operator!(const pbgForm & form);

pbgForm pbgFormImply(const pbgForm & form1, const pbgForm & form2);

pbgForm pbgFormEq(const pbgForm & form1, const pbgForm & form2);

pbgForm pbgFormExist(const char* s, const pbgForm & form);

pbgForm pbgFormForall(const char* s, const pbgForm & form);

pbgForm pbgFormTrue ();
     
pbgForm pbgFormFalse ();
     
ostream & operator <<(ostream & os, const pbgForm & form);


// ***************** actions ************  une action est consideree comme un pbgTerm. Rmq : c'est le parser qui refoule ce qu'on sait pas encore gere (comme x' >= 3)

typedef struct _action
{
  char* ident;
  linearsumTree affect;  
} action;


typedef struct _actionlist
{
  action val;
  struct _actionlist* next;  
} actionlist;

action CreateAction (const char* i, linearsumTree tree);

ostream & operator <<(ostream & os, const action & ac);

actionlist CreateActionList (action a);

actionlist operator+(const actionlist & ac, const action & a);

ostream & operator <<(ostream & os, const actionlist & ac);


// ***************** transitions ************

typedef struct _transition
{
  char* name;
  char* from;
  char* to;
  pbgForm guard;
  actionlist action;  
}transition;

typedef struct _transitionlist
{
  transition val;
  struct _transitionlist* next;
}transitionlist;

transition CreateTrans (const char* name, const char* form, const char* to, pbgForm guard, actionlist action);

ostream & operator <<(ostream & os, const transition & ac);

transitionlist CreateTransList (transition a);

transitionlist operator+(const transitionlist & ac, const transition & a);

ostream & operator <<(ostream & os, const transitionlist & ac);



// ***************** Model ************

typedef struct _stringlist
{
  char* val;
  struct _stringlist* next;
}stringlist;

typedef struct _model 
{
  const char* name;
  stringlist vars;
  stringlist states;
  // et les invariants ?!
  transitionlist trans;
} model;

typedef struct _modellist 
{
  model val;
  struct _modellist* next;  
} modellist;

stringlist CreateStringList (const char* s);

stringlist operator+(const stringlist & sl, const char* s);

ostream & operator <<(ostream & os, const stringlist & sl);

bool stringListFind (const char* s, stringlist sl);

stringlist stringListRemoveFirst (stringlist sl);

model CreateModel (const char* name, stringlist vars, stringlist states, transitionlist trans);

ostream & operator <<(ostream & os, const model & mod);

modellist CreateModelList (model m);

int ModStateToInt (model m, const char* state);

modellist operator+(const modellist & ml, model m);

ostream & operator <<(ostream & os, const modellist & sl);












//******************************************
// ******************************** strategy
//******************************************









// ***************** procedure 

typedef  struct _procedure 
{
  char* name;
  int argc;
  char** argv;  
} procedure;


procedure CreateProc (const char* name, char *arg);
procedure CreateProc (const char* name, char *arg1, char *arg2);

ostream & operator <<(ostream & os, const procedure & proc);



// ***************** Transitions (pour strategy)
// Transitionlist est un  stringlist !!

typedef  struct _TransitionsBasiqueList 
{
  stringlist val; // t1.t2.t3 ou t1
  struct  _TransitionsBasiqueList* next;  
} TransitionsBasiqueList;

typedef  union _TransitionsVal 
{
  char* id; // variable
  TransitionsBasiqueList tl;   // ensemble
} TransitionsVal;

typedef  struct _Transitions 
{
  int type;
  TransitionsVal val;  
} Transitions;

Transitions CreateTransitions (const char* id);

Transitions CreateTransitions (TransitionsBasiqueList sl);

TransitionsBasiqueList CreateTransitionsBasiqueList (stringlist sl);

TransitionsBasiqueList AddTransitionsBasiqueList (stringlist sl, TransitionsBasiqueList tbl);

ostream & operator <<(ostream & os, const TransitionsBasiqueList & trans);

ostream & operator <<(ostream & os, const Transitions & trans);

// ***************** region

typedef union  _regionTermVal   
{
  char* id;  
  pbgForm form;  
} regionTermVal;

typedef struct _regionTerm
{
  int type;  
  regionTermVal val;
} regionTerm;

regionTerm CreateRegionTerm(const char* id);

regionTerm CreateRegionTerm(pbgForm pbgf);

ostream & operator <<(ostream & os, const regionTerm & rt);


typedef struct _regFun
{
  char* fun;
  Transitions trans;
  int k;  
} regFun;


typedef union  _regionVal   
{
  char* op; // operator and ident of quantifier 
  regionTerm term;
  regFun fun;
} regionVal;


typedef struct  _region   
{
  int type;  
  regionVal val;  
  struct _region* left;
  struct _region* right;  
} region;

region CreateRegion (regionTerm rt);

region CreateRegion (const char* op, region right);

region CreateRegion (const char* op,region left,region right);

region CreateRegion (const char* id, Transitions trans, region reg, const char* k="1");

region operator&&(const region & reg1, const region & reg2);

region operator||(const region & reg1, const region & reg2);

region operator!(const region & reg);

region exist (const char* id, const region & reg);

region forall (const char* id, const region & reg);

ostream & operator <<(ostream & os, const region & reg);


// ***************** boolean 



typedef struct _boolTerm
{
  int type;  
  char* id;
  region* regleft;
  region* regright;
} boolTerm;

boolTerm CreateBoolTerm (const char* id);
boolTerm CreateBoolTerm (const char* id, region regright);
boolTerm CreateBoolTerm (const char* id, region regleft, region regright);



ostream & operator <<(ostream & os, const boolTerm & blt);



typedef union _boolVal 
{
  boolTerm term;
  const char* op;
} boolVal;


typedef struct  _boolForm   // formule de Presburger complete
{
  int type;
  boolVal val;
  struct _boolForm* left;
  struct _boolForm* right;  
} boolForm;

boolForm CreateBoolForm (boolTerm blt);

boolForm boolFormEqu(const boolForm & form1, const boolForm & form2);

boolForm operator&&(const boolForm & form1, const boolForm & form2);

boolForm operator||(const boolForm & form1, const boolForm & form2);

boolForm operator!(const boolForm & form);
     
ostream & operator <<(ostream & os, const boolForm & form);


// ***************** declaration

typedef union _varVal
{
  boolForm boolF;
  Transitions trans;
  region reg;  
} varVal;



typedef struct _declaration
{
  char* id;
  int type;
  varVal val;
} declaration;

declaration CreateDeclaration(const char* id, boolForm bf);

declaration CreateDeclaration(const char* id, Transitions tr);

declaration CreateDeclaration(const char* id, region reg);

ostream & operator <<(ostream & os, const declaration & decl);


// ***************** if_then_else


typedef struct _ifThenElse 
{
  boolForm cond;
  struct _instructionlist* If;
  struct _instructionlist* Else;
} ifThenElse;

ifThenElse CreateIfThenElse (boolForm bf,   struct _instructionlist  If,   struct _instructionlist Else);

ifThenElse CreateIfThenElse (boolForm bf,   struct _instructionlist  If);

ostream & operator <<(ostream & os, const ifThenElse & ite);

// ****************** instruction


typedef union _instructionVal 
{
  declaration decl;
  procedure proc;  
  ifThenElse ite;  
} instructionVal;

typedef struct _instruction 
{
  int type;
  instructionVal val;
} instruction;



instruction CreateInstruction (declaration decl);

instruction CreateInstruction (procedure proc);

instruction CreateInstruction (ifThenElse ite);

ostream & operator <<(ostream & os, const instruction & i);


// ****************** instructionlist

typedef struct _instructionlist 
{
  instruction i;  
  struct _instructionlist* next;  
} instructionlist;


instructionlist CreateInstructionList (instruction i);

instructionlist operator+(const instructionlist & il, instruction i);

instructionlist append(instructionlist head, instructionlist tail);

ostream & operator <<(ostream & os, const instructionlist & il);

// ****************** strategy

typedef struct _strategy 
{
  char* name;
  instructionlist val;  
} strategy;

strategy CreateStrategy(const char* id, instructionlist il);

ostream & operator <<(ostream & os, const strategy & s);

// ****************** modelStrategy

typedef struct _modelStrategy 
{
  model mod;
  strategy strateg;  
} modelStrategy;

modelStrategy CreateModelStrategy(model mod, strategy str);

ostream & operator <<(ostream & os, const modelStrategy & s);


#endif
