/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "Relation.h"

Relation::Relation(const LNDD &lndd, const Order &order):LNDD(lndd),unprime(order)
{
  //Il faudrait tester que les ordres sont compatibles ... BOF
  //A FAIRE
}

Order Relation::makeRelationOrder(const Order &order)
{
  Order global;

  for(int i=0;i<(int)(order.size());i++) {
    global.push_back(PrimeVariables::prime(order[i]));
    global.push_back(order[i]);
  }

  
  return global;
}

Relation Relation::settounprimeRelation(const LNDD &lndd) // nos relations ont la forme x_0' x_0 x_1' x_1 ... ici on met les contraintes sur x_o, ..., x_n et pas de contrainte sur x_0' ... x_n'
{
  int m = (int)(lndd.order.size()); 
      
//     int map[m];
//     for(int j=0;j<m;j++){
//       map[j]=2*j+1;
//     }

  int *selection = (int *)calloc(sizeof (int), 2*m);
  // on remplit selection
  for(int j=0;j<m;j++){
    selection[2*j]=1;      
    selection[2*j+1]=0;
  }
  // fin remplissage
    
  genepi_set* a1 = genepi_set_invproject(GENEPI_SOLVER,lndd.a,selection,
					 2*m);  
  Order global = makeRelationOrder(lndd.order);
  free (selection);

  return Relation(LNDD(a1,global),lndd.order);
}


Relation Relation::settoprimeRelation(const LNDD &lndd) // on met les contraintes sur x_0'...x_n' et rien sur x_0 ... x_n
{
  int m = (int)(lndd.order.size());
  
  int *selection = (int *) calloc (sizeof (int), 2*m);    
  // on remplit selection
  for(int j=0;j<m;j++){
    selection[2*j]=0;      
    selection[2*j+1]=1;
  }
  // fin remplissage
    
  genepi_set* a1 = genepi_set_invproject(LNDD::GENEPI_SOLVER,lndd.a,selection,2*m); 
  Order global = makeRelationOrder(lndd.order);
  free (selection);

  return Relation(LNDD(a1,global),lndd.order);
}



LNDD Relation::getridofprime() const 
{
  int m = (int)(order.size()/2);
    
  int *selection = (int *) calloc (sizeof (int), 2*m);

  // on remplit selection
  for(int j=0;j<m;j++){
    selection[2*j]=1;      
    selection[2*j+1]=0;
  }
  // fin remplissage
  
  genepi_set* a1 = genepi_set_project(LNDD::GENEPI_SOLVER,a, selection, 2*m);
  free (selection);

  return LNDD(a1,unprime);
}


LNDD Relation::getridofunprime() const
{
  int m = (int)(order.size()/2);   
  int *selection = (int *) calloc (sizeof (int), 2*m);
  // on remplit selection
  for(int j=0;j<m;j++){
    selection[2*j]=0;      
    selection[2*j+1]=1;
  }
  // fin remplissage
  
  genepi_set* a1 = genepi_set_project(LNDD::GENEPI_SOLVER,a, selection, 2*m);
  free (selection);

  return LNDD(a1,unprime);
}  



Relation Relation::everything(const Order &order)
{
  return Relation(LNDD::everything(Relation::makeRelationOrder(order)),order);
}

Relation Relation::nothing(const Order &order)
{
  return Relation(LNDD::nothing(Relation::makeRelationOrder(order)),order);
}

Relation Relation::equality(const Order &order)
{
  LNDD res=LNDD::everything(Relation::makeRelationOrder(order));
  for(int i=0;i<(int)(order.size());i++)
    res=res&LNDD(LinearSum(order[i])-LinearSum(PrimeVariables::prime(order[i])),"==0",Relation::makeRelationOrder(order));
  return Relation(res,order);
}


Relation Relation::leq(const Order &order)
{
  LNDD res=LNDD::everything(Relation::makeRelationOrder(order));
  for(int i=0;i<(int)(order.size());i++)
    res=
      res&
      LNDD(LinearSum(order[i])-LinearSum(PrimeVariables::prime(order[i])),">=0", Relation::makeRelationOrder(order))&
      LNDD(LinearSum(order[i]),">=0",Relation::makeRelationOrder(order))&
      LNDD(LinearSum(PrimeVariables::prime(order[i])),">=0",Relation::makeRelationOrder(order));
  
  return Relation(res,order);
}





Relation Relation::operator&(const Relation &relation) const
{
  return Relation( LNDD::operator&(relation),unprime);
}

Relation Relation::operator|(const Relation &relation) const
{
  return Relation( LNDD::operator|(relation),unprime);
}

Relation Relation::operator-(const Relation &relation) const
{
  return Relation( LNDD::operator-(relation),unprime);
}

Relation Relation::operator!(void) const
{
  return Relation( LNDD::operator!(),unprime);
}





Relation compose(const Relation &relation2, const Relation &relation1)  // compute R2*R1 (first R2 then R1)
{  


  int m = (int)(relation1.order.size()/2);  
  int *selection = (int *) calloc (sizeof (int), 3*m);


  for(int j=0;j<m;j++){
    selection[3*j+2] = 1;
    selection[3*j+1] = 0;
    selection[3*j] = 0;      
  }
  genepi_set* r1 = genepi_set_invproject(LNDD::GENEPI_SOLVER,relation1.a,
					 selection,3*m);
    
  
  // a1 = "s''\in R_1(s')"

  
  for(int j=0;j<m;j++){
    selection[3*j+2] = 0;
    selection[3*j+1] = 0;
    selection[3*j] = 1;    
  }
  genepi_set* r2 =  genepi_set_invproject(LNDD::GENEPI_SOLVER,relation2.a,
					  selection,3*m); 
  // a2 = " s'\in R_2(s) "


  
  genepi_set* equ=genepi_set_intersection(LNDD::GENEPI_SOLVER,r1,r2); 
  // equ= " s''\in R_1(s') &  s'\in R_2(s) "
  
  // On suprime s
    for(int j=0;j<m;j++){
      selection[3*j+2] = 0;
      selection[3*j+1] = 1;
      selection[3*j] = 0;    
    }
  genepi_set* result =  genepi_set_project(LNDD::GENEPI_SOLVER,equ,selection,
					   3*m); 
  // equ = s''\in R_1R_2(s)
  
  genepi_set_del_reference(LNDD::GENEPI_SOLVER,r1);
  genepi_set_del_reference(LNDD::GENEPI_SOLVER,r2);
  genepi_set_del_reference(LNDD::GENEPI_SOLVER,equ);

  free (selection);

  return Relation(LNDD(result,relation1.order),relation1.unprime);
}

LNDD
compose(const Relation &relation , const LNDD     &set)
{ 
  return LNDD(genepi_set_apply(LNDD::GENEPI_SOLVER,relation.a,set.a),set.order);
  //return (relation&Relation::settounprimeRelation(set,relation.prime)).getridofunprime(); 	 
}
  	 
LNDD
compose(const LNDD     &set      , const Relation &relation)
{ 	 
  return LNDD(genepi_set_applyinv(LNDD::GENEPI_SOLVER,relation.a,set.a),set.order);
  //return (relation&Relation::settoprimeRelation(set,relation.prime)).getridofprime(); 	 
} 	 
