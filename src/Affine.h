/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef AFFINE_H
#define AFFINE_H

#include "LNDD.h"
#include "Matrix.h"
#include "Relation.h"

class Affine
{  
 public:
  Order order;
  LNDD guard;
  SquareMatrix M;
  Vector v;
  string name;

 public:
  Affine(const SquareMatrix & M, 
         const Vector & v, 
         const LNDD & guard, 
         const Order &order,
         const string & name);
  

  bool positive() const;
  Affine dc() const;  
  Affine removeGuard(void) const;
  
  static Affine identity(const Order &order);
  
  Relation toRelation(void) const;
  Relation startoRelation(void) const;
  Relation convexStartoRelation(void) const;
  Relation positiveStartoRelation(void) const;
};

ostream & operator<<(ostream & os, const Affine &f);

Affine compose(const Affine & f1, const Affine &f2);

bool operator==(const Affine &f1, const Affine &f2);


#endif
