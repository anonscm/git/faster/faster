/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "Presburger.h"
//extern "C"{
//#include "dfa.h"
//}
//#include "Affine.h"
#include <iostream>
//#include "Dfa.h"

DFA *DFAplus;  /* p_2 = p_0 + p_1 */
DFA *DFAeq;    /* p_0 = p_1       */
//DFA *DFA2times;/* p_0 = 2.p_1     */
//DFA *DFAless;  /* p_0 < p_1 */
//DFA *DFAmore;  /* p_0 > p_1 */
DFA *DFA1;     /* no condition */
DFA *DFA0;     /* emptyset */

void fast_init() 
{
  bdd_init();

  //Construction de DFAplus
  {
    int indices[3];
    indices[0]=0;
    indices[1]=1;
    indices[2]=2;
    dfaSetup(4,3,indices);
  }
  // Construction de l'etat 0
  dfaAllocExceptions(0);
  dfaStoreState(1);
  // Construction de l'etat 1
  dfaAllocExceptions(4);
  dfaStoreException(1,"000");
  dfaStoreException(1,"011");
  dfaStoreException(1,"101");
  dfaStoreException(3,"110");
  dfaStoreState(2);
  // Construction de l'etat 2
  dfaAllocExceptions(0);
  dfaStoreState(2);
  // Construction de l'etat 3
  dfaAllocExceptions(4);
  dfaStoreException(3,"111");
  dfaStoreException(3,"100");
  dfaStoreException(3,"010");
  dfaStoreException(1,"001");
  dfaStoreState(2);
  DFAplus=dfaBuild("0+--");
  //  {
  // char **vars; // 0 terminated array of variable names 
  //  int *orders; // array of variable orders (0/1/2)
  //  DFAplus = dfaImport("presburger_plus_012.dfa", &vars, &orders);
  //}


  //  {
  //  char **vars; // 0 terminated array of variable names
  //  int *orders; // array of variable orders (0/1/2)
  //  DFA2times = dfaImport("presburger_2times_01.dfa", &vars, &orders);
  //}



  //Construction de DFAeq
  {
    int indices[2];
    indices[0]=0;
    indices[1]=1;
    dfaSetup(3,2,indices);
  }
  // Construction de l'etat 0
  dfaAllocExceptions(0);
  dfaStoreState(1);
  // Construction de l'etat 1
  dfaAllocExceptions(2);
  dfaStoreException(1,"00");
  dfaStoreException(1,"11");
  dfaStoreState(2);
  // Construction de l'etat 2
  dfaAllocExceptions(0);
  dfaStoreState(2);  
  DFAeq=dfaBuild("0+-");    
  //  {
  //  char **vars; // 0 terminated array of variable names
  //  int *orders; // array of variable orders (0/1/2)
  //  DFAeq = dfaImport("presburger_eq_01.dfa", &vars, &orders);
  //}


    
  //  {
  //  char **vars; // 0 terminated array of variable names
  //  int *orders; // array of variable orders (0/1/2)
  //  DFAless = dfaImport("presburger_less_01.dfa", &vars, &orders);
  //} 
  
  //{
  //  char **vars; // 0 terminated array of variable names
  //  int *orders; // array of variable orders (0/1/2)
  //  DFAmore = dfaImport("presburger_more_01.dfa", &vars, &orders);
  //} 
  




  //{
  //  char **vars; // 0 terminated array of variable names
  //  int *orders; // array of variable orders (0/1/2)
  //  DFA1 = dfaImport("presburger_1_01.dfa", &vars, &orders);
  //}

  //Construction de DFA1 et DFA0
  DFA1=dfaTrue();
  DFA0=dfaFalse();

   
  //{
  //DFA *a1,*a2,*a3;
  //a1=dfaTrue();
  //a2=dfaFalse();
  //a3=dfaProduct(a1,a2,dfaAND);
  //DFA0=dfaMinimize(a3);
  //dfaFree(a1);
  //dfaFree(a2);
  //dfaFree(a3);
  //}
  
}

void fast_close()
{
  dfaFree(DFAplus);
  dfaFree(DFAeq);
  //dfaFree(DFA2times);
  //dfaFree(DFAless);
  dfaFree(DFA1);
  dfaFree(DFA0);
}


// On construit le DFA " p_i=p_j "
DFA *dfaPresburgerEq(const int i, const int j) 
{
  DFA *a;
  if (i<j) {
    a=dfaCopy(DFAeq);
    int map[2];
    map[0]=i;
    map[1]=j;
    dfaReplaceIndices(a,map);
    return a;    
  } else if (j<i) {
    a=dfaCopy(DFAeq);    
    int map[2];
    map[0]=j;
    map[1]=i;
    dfaReplaceIndices(a,map);
    return(a);
  } else { // (i==j)
    a=dfaCopy(DFA1);
    return a;    
  }
}


int max3(const int i,const int j, const int k) 
{
  int m=i;
  if (m<j) m=j;
  if (m<k) m=k;
  return m;  
}



// On construit le DFA " p_i+p_j=p_k "
DFA *dfaPresburgerPlus(const int i, const int j, const int k)
{
  DFA *a, *a2, *a3, *a4, *a5, *ai, *aj, *ak;
  int m;
  
  // We select a large m such that \{m, m+1, m+2\}\cap\{i,j,k\}=\emptysey
  m=max3(i,j,k)+1;
  
  a=dfaCopy(DFAplus);
  {
    int map[3];
    map[0]=m;
    map[1]=m+1;
    map[2]=m+2;
    dfaReplaceIndices(a,map);
  } // a = " p_{m+2}=p_{m+1}+p_m "
  
  ai=dfaPresburgerEq(i,m);  
  a2=dfaProduct(a,ai,dfaAND);
  a3=dfaMinimize(a2);
  dfaRightQuotient(a3, m);
  a4 = dfaProject(a3, m);
  a5 = dfaMinimize(a4);
  dfaFree(a);
  dfaFree(a2);
  dfaFree(a3);
  dfaFree(a4);
  dfaFree(ai);  
  a=a5; // a = " p_{m+2}=p_{m+1}+p_i "
  
  aj=dfaPresburgerEq(j,m+1);
  a2=dfaProduct(a,aj,dfaAND);
  a3=dfaMinimize(a2);
  dfaRightQuotient(a3, m+1);
  a4 = dfaProject(a3, m+1);
  a5 = dfaMinimize(a4);
  dfaFree(a);
  dfaFree(a2);
  dfaFree(a3);
  dfaFree(a4);
  dfaFree(aj);
  a=a5; // a = " p_{m+2}=p_j+p_i "  
  
  ak=dfaPresburgerEq(k,m+2);  
  a2=dfaProduct(a,ak,dfaAND);
  a3=dfaMinimize(a2);
  dfaRightQuotient(a3, m+2);
  a4 = dfaProject(a3, m+2);
  a5 = dfaMinimize(a4);
  dfaFree(a);
  dfaFree(a2);
  dfaFree(a3);
  dfaFree(a4);
  dfaFree(ak);  
  a=a5; // a = " p_k=p_j+p_i "    
  
  return a;
}


// Construit le DFA minimal qui represente p_0=x.p_1 ou x est un entier >=0

DFA *dfaxtimes(const int x)
{
  DFA* a1, *a2, *ar;
  
  a1=dfaPresburgerEq(0,1); // "p_0=p_1"
  a2=dfaPresburgerPlus(2,2,1); // "p_2+p_2=p_1"
  ar=dfaPresbConst(0,0); // "p_0=0"

// i est le nombre de fois que la boucle a ete executee 
//        a1 = "p_0=2^i.p_1"      
//        a2 = "p_1=2.p_2"    
// on ecrit x=x_0.2^0+...+x_n.2^n avec x_i\in\{0,1\} 
// On a   ar= " p_0=(x_0.2^0+...+x_{i-1}.2^{i-1}).p_3" 
  
  int y=x;
  while (y!=0) {
//      a1 = "p_0=2^i.p_1"   
    if ((y%2)!=0) {
// on donne a a1 la valeur "p_1=2^i.p_3" 
      DFA *a3, *a4, *a5, *a6, *a7, *a8, *a9, *a10;      
      {
        int map[2];
        map[0]=1;
        map[1]=3;
        dfaReplaceIndices(a1, map);
      }
// a4=a3= " p_1=2^i.p_3 & p_2=p_0+p_1" 
      a3 = dfaProduct(a1, DFAplus, dfaAND);
      a4 = dfaMinimize(a3);
// On redonne a a1 la valeur "p_0=2^i.p_1"
      {
        int map[4];
        map[3]=1;
        map[1]=0;
        dfaReplaceIndices(a1, map);
      }
// a6=a5="p_2=p_0+2^i.p_3 " = "ex p1 :  p_1=2^i.p_3 & p_2=p_0+p_1"
      dfaRightQuotient(a4, 1);
      a5 = dfaProject(a4, 1);
      a6 = dfaMinimize(a5);        

// a8=a7 = "p_2=p_0+2^i.p_3 & p_0=(x_0.2^0+...+x_{i-1}.2^{i-1}).p_3 "
      a7 = dfaProduct(a6, ar, dfaAND);
      a8 = dfaMinimize(a7);
// a10=a9 = "p_2=(x_0.2^0+...+x_i.2^i).p_3" = "ex p_0 : p_2=p_0+2^i.p_3 & p_0=(x_0.2^0+...+x_{i-1}.2^{i-1}).p_3 "
      dfaRightQuotient(a8, 0);
      a9 = dfaProject(a8, 0);
      a10 = dfaMinimize(a9);
// On modifie a10 pour qu'il est la valeur "p_0=(x_0.2^0+...+x_i.2^i).p_3"
      {
        int map[4];
        map[2]=0;
        map[3]=3;
        dfaReplaceIndices(a10, map);
      }
// On libere la memoire
      dfaFree(a3);
      dfaFree(a4);
      dfaFree(a5);
      dfaFree(a6);
      dfaFree(a7);
      dfaFree(a8);
      dfaFree(a9);
      dfaFree(ar);
      ar=a10;
// ici, on ar = "p_0=(x_0.2^0+...+x_i.2^i).p_3"
    }
    
    y=y/2;
    
    {
      DFA *a3, *a4, *a5, *a6;
      
//      a4 = a3 = "p_0=2^i.p_1 & p_1=2.p_2"  
      a3 = dfaProduct(a1, a2, dfaAND);
      a4 = dfaMinimize(a3);   
//      a6= a5 = "ex p_1 : p_0=2^i.p_1 & p_1=2.p_2 
      dfaRightQuotient(a4, 1);
      a5 = dfaProject(a4, 1);
      a6 = dfaMinimize(a5);  
// a6 = " p_0=2^{i+1}.p_2 " 
      {
        int map[3];
        map[2]=1;
        map[0]=0;
        dfaReplaceIndices(a6, map);
      }
// a6 =" p_0=2^{i+1}.p_1 "
// On libere la memoire 
      dfaFree(a3);
      dfaFree(a4);
      dfaFree(a5);
      dfaFree(a1);// AJOUTE le 29-01-2002 par Jerome Leroux pour eviter les fuites memoires
      a1=a6;
    }
  }
// on a alors ar = "p_0=x.p_3
  // on modie ar pour que ar ="p_0=x.p_1"
  {
    int map[4];
    map[3]=1;
    map[0]=0;
    dfaReplaceIndices(ar, map);
  }
  dfaFree(a2);
  dfaFree(a1);
  return ar;
}



// Construit le DFA minimal qui represente p_2=p_1 + x.p_0  ou x>=0 
DFA *dfaPresburgerPlusExtended(const int n)
{
  DFA *a0,*a1, *a2, *a3, *a4, *a5;    
  a1=dfaxtimes(n); // a1= " p_0=n.p_1 "
  a0=dfaPresburgerPlus(0,2,3);  //" p_3=p_0+p_2 "
  a2 = dfaProduct(a1, a0, dfaAND); // a2 = " p_3=p_0+p_2 & p_0=n.p_1"
  a3 = dfaMinimize(a2);
  dfaRightQuotient(a3, 0);
  a4 = dfaProject(a3, 0); // a4= " p_3=p_2+n.p_1 "
  a5 = dfaMinimize(a4); 
  {
    int map[4];
    map[3]=2;
    map[1]=0;
    map[2]=1;
    dfaReplaceIndices(a5, map);
  }// a5 = " p_2=p_1+n.p_0 "
  
  dfaFree(a0);    
  dfaFree(a1);
  dfaFree(a2);
  dfaFree(a3);
  dfaFree(a4);
  return a5; // "p_2=p_1+n.p_0 "
}


// On se donne un entier $n\geq0$ et deux liste $pos[i]$ et $val[i]$ 
//   On construit le DFA representant $\sum_{i=0}^{n-1}val[i].p_{pos[i]}=c$
DFA *dfaPresburgerEquation(const int *val, const int *pos, const int c, const int n)
{
  // soit j le nombre de fois que la boucle for a ete executee. On note a le DFA qui represente " $p_m=\sum_{i=0}^{j-1}val[i].p_{pos[i]}$. Initialiement, on a donc a= " p_n=0 "


  int m;  
  DFA *dfapos,*dfaneg;
  // on calcule un m assez grand pour que \forall j pos[j]<m.
  m=0;
  for(int j=0;j<n;j++)
    if (pos[j]>m)
      m=pos[j];
  m=m+1;
  //cout << "m="<<m<<endl;
  
  int map[m+2];
  for(int j=0;j<m;j++)
    map[j]=j;
  map[m+1]=m;
  if (c>=0){
    dfapos=dfaPresbConst(m,0);
    dfaneg=dfaPresbConst(m,c);         
  } else {
    dfapos=dfaPresbConst(m,-c);
    dfaneg=dfaPresbConst(m,0);         
  }
  for(int j=0;j<n;j++){
    DFA *a1,*a2,*a3,*a4,*a5;       
    if (val[j]>0) {
      a1=dfaPresburgerPlusExtended(val[j]); // " p_2=p_1+val[j].p_0 "
      {
        int map[3];
        map[2]=m+1;
        map[1]=m;
        map[0]=pos[j];
        dfaReplaceIndices(a1,map);
      } // a1 = "p_{m+1}=p_m+val[j].p_{pos[j]} "
      a2=dfaProduct(dfapos,a1,dfaAND);
      a3=dfaMinimize(a2);
      dfaRightQuotient(a3, m);
      a4 = dfaProject(a3, m); // a4= " p_{m+1}=\sum_{i=0}^j val[i].p_{pos[i]} "
      a5 = dfaMinimize(a4);     
      dfaReplaceIndices(a5,map); 
      dfaFree(a1);
      dfaFree(a2);
      dfaFree(a3);
      dfaFree(a4);
      dfaFree(dfapos);
      dfapos=a5;          
    } else {
      a1=dfaPresburgerPlusExtended(-val[j]); // " p_2=p_1-val[j].p_0 "
      {
        int map[3];
        map[2]=m+1;
        map[1]=m;
        map[0]=pos[j];
        dfaReplaceIndices(a1,map);
      } // a1 = "p_{m+1}=p_m-val[j].p_{pos[j]} "
      a2=dfaProduct(dfaneg,a1,dfaAND);
      a3=dfaMinimize(a2);
      dfaRightQuotient(a3, m);
      a4 = dfaProject(a3, m); // a4= " p_{m+1}=\sum_{i=0}^j val[i].p_{pos[i]} "
      a5 = dfaMinimize(a4);     
      dfaReplaceIndices(a5,map); 
      dfaFree(a1);
      dfaFree(a2);
      dfaFree(a3);
      dfaFree(a4);
      dfaFree(dfaneg);
      dfaneg=a5;              
    }
  }
  
  {
    DFA *a2, *a3, *a4, *a5;
    a2=dfaProduct(dfapos,dfaneg,dfaAND);
    a3=dfaMinimize(a2);
    dfaRightQuotient(a3, m);
    a4 = dfaProject(a3, m); // a4= " \sum_{i=0}^{n-1} val[i].p_{pos[i]}=c "
    a5 = dfaMinimize(a4);     
    dfaFree(a2);
    dfaFree(a3);
    dfaFree(a4);
    dfaFree(dfapos);
    dfaFree(dfaneg);
    return a5;    
  }

}
