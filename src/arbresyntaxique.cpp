/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "arbresyntaxique.h"


/* des fonctions qui servent  */


bool isinteger (const char* s, int* i)
{
  bool result;
  char** tail = (char**)malloc(sizeof(char*));  
  int r = (int)strtol(s, tail, 10);  
  if (strlen(*tail)>=1) 
    {result=false; r=0;}
  else { result=true;}
  *i=r;
  free(tail);  
  return result;
}
#include <cmath>
char* intTostring (int i)
{
  size_t sz = i == 0 ? 2 : log10 (abs (i)) + 2;
  char* s = (char*) malloc (sz * sizeof(char));
  sprintf(s, "%d", i);  
  return s; 
}


/* deux structures presburgerTree et linearsumTree pour representer les equations de Presburger et des sommes lineaires */
/* et une structure pour representer un modele */


// ******************* linearsumtree ********************

linearsumTree stringToLstree (const char* s)
{
  linearsumTree t;
  t.val = strdup(s);
  t.left = NULL;
  t.right = NULL;    
  return t;
}



linearsumTree operator+(const linearsumTree &t1, const linearsumTree &t2)
{
  linearsumTree t;
  t.val = strdup("+");
    
  t.left = (linearsumTree*)malloc(sizeof(linearsumTree));
  t.right = (linearsumTree*)malloc(sizeof(linearsumTree));    

  *(t.left) = t1;
  *(t.right) = t2;
    
  return t;  
}


linearsumTree operator-(const linearsumTree &t1, const linearsumTree &t2)
{
  linearsumTree t;
  t.val = strdup("-");
    
  t.right = (linearsumTree*)malloc(sizeof(linearsumTree));
  t.left = (linearsumTree*)malloc(sizeof(linearsumTree));    

  *(t.left) = t1;
  *(t.right) = t2;
    
  return t;  
}


linearsumTree operator*(const char* s, const linearsumTree &t2)
{
  linearsumTree t;
  t.val = strdup("*");
    
  t.right = (linearsumTree*)malloc(sizeof(linearsumTree));
  t.left = (linearsumTree*)malloc(sizeof(linearsumTree));    

  *(t.left) = stringToLstree(s);
  *(t.right) = t2;
    
  return t;  
}

linearsumTree operator-(const linearsumTree &t1)
{    
  return "-1"*t1;  
}


linearsumTree operator*(int i, const linearsumTree &t2)
 {
   char* s = intTostring (i);

   return s*t2;  
 }
 




ostream & operator <<(ostream & os, const linearsumTree & t)
{  
  bool ismult = (strcmp(t.val,"*")==0);
  
  if (t.left != NULL) os<<*(t.left);
  
  os<<t.val;
  if (ismult) os<<"(";  
  if (t.right != NULL) os<<*(t.right);
  if (ismult) os<<")";    
  return (os<<"");   // oblige de le mettre sinon core dump, bizarre .... peut etre le recursif qui passe mal  
}


// ******************* pbgTerm ********************

pbgTerm pbgTermCreate (linearsumTree lt, const char* opt)
{
  pbgTerm pbgt;
  pbgt.lsum = lt;
  pbgt.op = strdup(opt);
  return pbgt;
}

ostream & operator <<(ostream & os, const pbgTerm & pbgt)
{  
  os << pbgt.lsum << " " <<pbgt.op<< " ";
  return (os<<"");
}


// ******************* pbgFormula *****************


pbgForm termToForm (pbgTerm pbgt)
{
  pbgForm res;
  res.type=1;
  res.val.term=pbgt;
  res.left=NULL;
  res.right=NULL; 
  return res;
}

pbgForm pbgFormState (char* s)
{
  pbgForm res;
  res.type=4;
  res.val.state = (char*) malloc(sizeof(char));
  res.val.state=strdup(s);
  res.left=NULL;
  res.right=NULL; 
  return res;  
}



pbgForm operator&&(const pbgForm & form1, const pbgForm & form2)
{
  pbgForm f;
  f.type=2;
  f.val.op = strdup("AND");
    
  f.left = (pbgForm*)malloc(sizeof(pbgForm));
  f.right = (pbgForm*)malloc(sizeof(pbgForm));    

  *(f.left) = form1;
  *(f.right) = form2;
    
  return f;  
}

pbgForm operator||(const pbgForm & form1, const pbgForm & form2)
{
  pbgForm f;
  f.type=2;
  f.val.op = strdup("OR");
    
  f.left = (pbgForm*)malloc(sizeof(pbgForm));
  f.right = (pbgForm*)malloc(sizeof(pbgForm));    

  *(f.left) = form1;
  *(f.right) = form2;

  return f;
}

pbgForm operator!(const pbgForm & form)
{
  pbgForm f;
  f.type=2;
  f.val.op = strdup("NO");
    
  f.right = (pbgForm*)malloc(sizeof(pbgForm));
  f.left = NULL;
  
  *(f.right) = form;
    
  return f;
}





pbgForm pbgFormImply(const pbgForm & form1, const pbgForm & form2)
{
  pbgForm f;
  f = (!form1)||form2;
  return f;
}

pbgForm pbgFormEq(const pbgForm & form1, const pbgForm & form2)
{
  pbgForm f;
  f = pbgFormImply(form1, form2)&&pbgFormImply(form2, form1);
  return f;
}

pbgForm pbgFormExist(const char* s, const pbgForm & form)
{
  pbgForm f;
  f.type = 3;
  f.val.quant.q = strdup("exists");
  f.val.quant.ident = strdup(s);

  f.left = NULL;
  f.right =  (pbgForm*)malloc(sizeof(pbgForm));

  *(f.right) = form;
  return f;
}

pbgForm pbgFormForall(const char* s, const pbgForm & form)
{
  pbgForm f;
  f = !pbgFormExist(s, !form);
  return f;
}



pbgForm pbgFormTrue ()
{
  pbgForm res;
  res.type=2;
  res.val.op=strdup("true");
  res.left=NULL;
  res.right=NULL; 
  return res;
}

pbgForm pbgFormFalse ()
{
  pbgForm res;
  res.type=2;
  res.val.op=strdup("false");
  res.left=NULL;
  res.right=NULL; 
  return res;
}

ostream & operator <<(ostream & os, const pbgForm & form)
{    
  if (form.left != NULL) os<<"("<<*(form.left)<<")";  
  if (form.type == 1) {os<<form.val.term;} else if (form.type==2) {os<<form.val.op ;} else if (form.type==3) {os<<form.val.quant.q<<" "<<form.val.quant.ident<<". ";} 
  else if (form.type==4) {os<<"state = "<<form.val.state;} else cerr<<"unknown pbgForm type";
  
  if (form.right != NULL) os<<"("<<*(form.right)<<")";    
    return (os<<"");   // oblige de le mettre sinon core dump, bizarre .... peut etre le recursif qui passe mal
  
}



// *************************** actions *************************


action CreateAction (const char* id, linearsumTree t)
{
  action ac;
  ac.ident = strdup(id);
  ac.affect = t;
  return ac;
}

ostream & operator <<(ostream & os, const action & ac)
{
  os<<ac.ident<<" := "<<ac.affect ;
    return (os<<"");  
}


actionlist CreateActionList (action a)
{
  actionlist ac;  
  ac.val = a;  
  ac.next = NULL;
  return ac;
}


actionlist operator+(const actionlist & act, const action & a)  // mise en tete
{
  actionlist ac;
  ac.val = a;
  ac.next = (actionlist*)malloc(sizeof(actionlist));
  *(ac.next) = act;
  return ac;
}


ostream & operator <<(ostream & os, const actionlist & ac)
{
  os<<"\t"<<ac.val<<" ; "<<endl;
  if (ac.next != NULL) os<<*(ac.next);
    return (os<<"");
}

// ***************** transitions ************

transition CreateTrans (const char* name, const char* from, const char* to, pbgForm guard, actionlist action)
{
  transition trans;
  trans.name = strdup(name);
  trans.from = strdup(from);
  trans.to = strdup(to);
  trans.guard = guard;
  trans.action = action;  
  return trans;  
}

ostream & operator <<(ostream & os, const transition & trans)
{
  os<<"transition "<<trans.name<<" {"<<endl<<"from := "<<trans.from<<";\n"<<"to := "<<trans.to<<";\n"<<"guard := "<<trans.guard<<";\naction := \n"<<trans.action<<"}";  
  return (os<<"");
}

transitionlist CreateTransList (transition a)
{
  transitionlist tl;
  tl.val = a;
  tl.next = NULL;
  return tl;
}

transitionlist operator+(const transitionlist & ac, const transition & a)
{
  transitionlist tl;
  tl.val = a;
  tl.next = (transitionlist*)malloc(sizeof(transitionlist));
  *(tl.next) = ac;  
  return tl;
}

ostream & operator <<(ostream & os, const transitionlist & ac)
{
  os<<ac.val<<';'<<endl<<endl;
  if (ac.next != NULL) os<<*(ac.next);
    return (os<<"");
}

// *************************** Modele *************************

stringlist CreateStringList (const char* s)
{
  stringlist sl;
  sl.val = strdup(s);
  sl.next = NULL;
  return sl;  
}

stringlist operator+(const stringlist & sl, const char* s)
{
  stringlist res;
  res.val = strdup(s);
  res.next = (stringlist*)malloc(sizeof(stringlist));
  *(res.next) = sl;
  return res;  
}


bool stringListFind (const char* s, stringlist sl)
{
  bool found = false;
  stringlist* n = (stringlist*)malloc(sizeof(stringlist));
  *n = sl;
  
  while(n!=NULL && !found ) {
    found = (strcmp(n->val, s)==0);
    n = n->next;    
  }
  return found; 
}

stringlist stringListRemoveFirst (stringlist sl) 
{
  return (*sl.next);  
}


ostream & operator <<(ostream & os, const stringlist & sl)
{
  os << sl.val;

  if ( sl.next != NULL )
    os << ", " << *(sl.next) ;

  return ( os << "" ) ;
}




//***************************************************

model CreateModel (const char* name, stringlist vars, stringlist states, transitionlist trans)
{
  model mod;
  mod.name = name;
  mod.vars = vars;
  mod.states = states;
  mod.trans = trans;
  return mod;  
}


int ModStateToInt (model m, const char* s)
{
  bool found = false;  
  bool  end = false;  
  stringlist sl = m.states;

  //  int i =1;
  int i=0; // modified by Sebastien Bardin on 31/07/2003.  
   while (!end && !found) {
     if (strcmp(s, sl.val )==0) {found=true;} else i++;     
     if (sl.next==NULL) end=true;
     else sl= *(sl.next);
   }        
   if (found) {return i;} else return -1;  
}


ostream & operator <<(ostream & os, const model & mod)
{
  return os<<"\n\nModel "<<mod.name<<"{\nvars "<<mod.vars<<";\n\nstates "<<mod.states<<";"<<endl<<endl<<mod.trans<<"\n}";    
}

modellist CreateModelList (model m)
{
  modellist ml;
  ml.val = m;
  ml.next = NULL;
  return ml;  
}


modellist operator+(const modellist & ml, model m)
{
  modellist res;
  res.val = m;
  res.next = (modellist*)malloc(sizeof(modellist));
  *(res.next) = ml;
  return res;
}


ostream & operator <<(ostream & os, const modellist & ml)
{
  os << ml.val<<endl;
  if (ml.next != NULL) os<<*(ml.next);
    return (os<<"");
}




//******************************************
// ******************************** strategy
//******************************************









// ***************** procedure 

/*typedef  struct _procedure 
{
  char* name;
  char* arg;  
} procedure;
*/

procedure  CreateProc (const char* name, char *arg)
{
  procedure res;
  res.name = strdup(name);  
  res.argc = 1; 
  res.argv = new char *[1];
  res.argv[0] = strdup (arg);

  return res;
}

			/* --------------- */

procedure CreateProc (const char* name, char *arg1, char *arg2)
{
  procedure res;
  res.name = strdup(name);  
  res.argc = 2; 
  res.argv = new char *[2];
  res.argv[0] = strdup (arg1);
  res.argv[1] = strdup (arg2);

  return res;
}

			/* --------------- */

ostream & operator <<(ostream & os, const procedure & proc)
{
  os << proc.name << " (";
  for (int i = 0; i < proc.argc; i++) {
    os << proc.argv[i];
    if (i < proc.argc - 1) {
      os << ", ";
    }
  }
  
  return (os << ")");
}

// ***************** boolTerm

/*typedef struct _boolTerm
{
  int type;  
  char* id;
  region* regleft;
  region* regright;
} boolTerm;

#define BOOLTRUE 1
#define BOOLFALSE 2
#define VAR 3
#define BINFUN 4
#define UNFUN 5
*/

boolTerm CreateBoolTerm (const char* id)
{
  boolTerm res;
  res.type=1;
  res.id=strdup(id);
  res.regleft=NULL;
  res.regright=NULL; 
  return res;  
}

boolTerm CreateBoolTerm (const char* id, region regleft, region regright)
{
  boolTerm res;
  res.type=3;
  res.id=strdup(id);

  res.regleft=(region*)malloc(sizeof(region));
  *(res.regleft)=regleft;

  res.regright=(region*)malloc(sizeof(region));
  *(res.regright)=regright;
 
  return res;  
}

boolTerm CreateBoolTerm ( const char* id, region regright)
{
  boolTerm res;
  res.type=2;
  res.id=strdup(id);
  res.regleft=NULL;

  res.regright=(region*)malloc(sizeof(region));
  *(res.regright) = regright;

  return res;  
}



ostream & operator <<(ostream & os, const boolTerm & blt)
{
  if (blt.type==1) {return (os<<blt.id);}
  else if (blt.type==3) {return (os<<blt.id<<"("<<*(blt.regleft)<<","<<*(blt.regright)<<")");}
  else if (blt.type==2) {return (os<<blt.id<<"("<<*(blt.regright)<<")");} 
  else return (os<<"error in boolterm"<<endl);  
}


// ***************** boolForm

/*
typedef union _boolVal 
{
  boolTerm term;
  char* op;
} boolVal;


typedef struct  _boolForm   // formule booleenne complete
{
  int type;
  boolVal val;
  struct _boolForm* left;
  struct _boolForm* right;  
} boolForm;
#define BOOLTERM 1
#define BOOLOP 2
*/

boolForm CreateBoolForm (boolTerm blt)
{
  boolForm res;
  res.type=1;
  res.val.term=blt;
  res.left=NULL;
  res.right=NULL;  
  return res;
  
}


boolForm operator&&(const boolForm & form1, const boolForm & form2)
{
  boolForm res;
  res.type=2;
  res.val.op="AND";
  
  res.left=(boolForm*)malloc(sizeof(boolForm));
  (*res.left)=form1;
  
  res.right=(boolForm*)malloc(sizeof(boolForm));
  (*res.right)=form2;      
  
  return res;
  
}

boolForm boolFormEqu(const boolForm & form1, const boolForm & form2)
{
  boolForm res;
  res.type=2;
  res.val.op="EQU";
  
  res.left=(boolForm*)malloc(sizeof(boolForm));
  (*res.left)=form1;
  
  res.right=(boolForm*)malloc(sizeof(boolForm));
  (*res.right)=form2;        
  
  return res; 
}

boolForm operator||(const boolForm & form1, const boolForm & form2)
{
  boolForm res;
  res.type=2;
  res.val.op="OR";
  
  res.left=(boolForm*)malloc(sizeof(boolForm));
  (*res.left)=form1;
  
  res.right=(boolForm*)malloc(sizeof(boolForm));
  (*res.right)=form2;        
  
  return res; 
}


boolForm operator!(const boolForm & form)
{
  boolForm res;
  res.type=2;
  res.val.op="NO";
  
  res.left=NULL;
  
  res.right=(boolForm*)malloc(sizeof(boolForm));
  (*res.right)=form;        
  
  return res;  
  
}

     
ostream & operator <<(ostream & os, const boolForm & form)
{
  if (form.type ==1) {os<<form.val.term;}
  else {os<<form.val.op<<'(';
        if (form.left!=NULL) {os<<*(form.left)<<",";}
	os<<*(form.right)<<')';
        }
  
  return (os<<"");
  
}


// ***************** Transitions

/*
typedef  union _TransitionsVal 
{
  char* id;
  stringlist tl;  
} TransitionsVal;

typedef  struct _Transitions 
{
  int type;
  TransitionsVal val;  
} Transitions;
// pour les Transitions (strategy)
#define ID 1
#define STRINGLIST 2
*/


Transitions CreateTransitions (const char* id)
{
  Transitions res;
  res.type=1;
  res.val.id=strdup(id);
  return res;  
}


Transitions CreateTransitions (TransitionsBasiqueList sl)
{
  Transitions res;
  res.type=2;
  res.val.tl=sl;
  return res;    
}


TransitionsBasiqueList CreateTransitionsBasiqueList (stringlist sl)
{
  TransitionsBasiqueList list;
  list.val = sl;
  list.next = NULL;
  return list;    
}


TransitionsBasiqueList AddTransitionsBasiqueList (stringlist sl, TransitionsBasiqueList tbl)
{
  TransitionsBasiqueList res;
  res.val = sl;
  res.next = (TransitionsBasiqueList*)malloc(sizeof(TransitionsBasiqueList));
  *(res.next) = tbl;
  return res;  
}



ostream & operator <<(ostream & os, const TransitionsBasiqueList & trans)
{
 
  os << trans.val ;

  if (trans.next != NULL)
    os << ";; " << *(trans.next) ;

   return (os<<"");
}


ostream & operator <<(ostream & os, const Transitions & trans)
{
  if (trans.type==1)  os<<trans.val.id;
  else if (trans.type==2)  os<<'{'<<trans.val.tl<<'}';
  return (os<<"");  
}


// ***************** region

/*
typedef union  _regionTermVal   
{
  char* id;  
  pbgForm form;  
} regionTermVal;


typedef struct _regionTerm
{
  int type;  
  regionTermVal val;
} regionTerm;

// regionTerm
#define ID 1
#define PBGFORM 2
*/



regionTerm CreateRegionTerm(const char* id)
{
  regionTerm res;
  res.type=1;
  res.val.id=strdup(id);
  return res;
}


regionTerm CreateRegionTerm(pbgForm pbgf)
{
  regionTerm res;
  res.type=2;
  res.val.form=pbgf;  
  return res;
}


ostream & operator <<(ostream & os, const regionTerm & rt)
{
  if (rt.type==1) os<<rt.val.id;
  else os<<rt.val.form;
  return os<<"";  
}

/*
typedef struct _regFun
{
  char* fun;
  Transitions trans;
  int k;  
} regFun;
typedef union  _regionVal   
{
  char* op;  
  regionTerm term;
  regFun fun;  
} regionVal;
typedef struct  _region   
{
  int type;  
  regionVal val;  
  struct _region* left;
  struct _region* right;  
} region;
// region
#define TERM 1
#define OP 2
#define FUNCTION 3
*/


region CreateRegion (regionTerm rt)
{
  region res;
  res.type=1;
  res.val.term=rt;
  res.left=NULL;
  res.right=NULL;
  return res;  
}

region CreateRegion (const char* id, region right)
{
  region res;
  res.type=2;
  res.val.op=strdup(id);

  res.left=NULL;

  res.right=(region*)malloc(sizeof(region));
  *(res.right)=right;

  return res;      
}

region CreateRegion (const char* id, region left, region right)
{
  region res;
  res.type=2;
  res.val.op=strdup(id);

  res.left=(region*)malloc(sizeof(region));
  *(res.left)=left;

  res.right=(region*)malloc(sizeof(region));
  *(res.right)=right;

  return res;      
}

region CreateRegion (const char* id, Transitions trans, region reg, 
		     const char* k)
{
  region res;
  res.type=3;
  res.val.fun.fun=strdup(id);
  res.val.fun.trans=trans;

  int* i =(int*)malloc(sizeof(int));
  isinteger(k,i);
  res.val.fun.k=*i;

  res.left=NULL;

  res.right=(region*)malloc(sizeof(region));
  *(res.right)=reg;

  return res;          
}

region operator&&(const region & reg1, const region & reg2)
{
  return CreateRegion("AND",reg1,reg2);    
}


region operator||(const region & reg1, const region & reg2)
{
  return CreateRegion("OR",reg1,reg2);      
}


region operator!(const region & reg)
{
  return CreateRegion("NO",reg);  
}

region exist (const char* id, const region & reg)
{
  region res;
  res.type=4;
  res.val.op=strdup(id);

  res.left=NULL;

  res.right=(region*)malloc(sizeof(region));
  *(res.right)=reg;

  return res;      
}

region forall (const char* id, const region & reg)
{
  return !exist(id, !reg);  
}


ostream & operator <<(ostream & os, const region & reg)
{
  if (reg.type==1) os<<reg.val.term;
  else if (reg.type==2) {os<<reg.val.op<<'('; 
                         if (reg.left!=NULL)  os<<*(reg.left)<<',';
                         os<<*(reg.right)<<')';
                         }
  else if (reg.type==3) {os<<reg.val.fun.fun<<'('<<*(reg.right) <<','<<reg.val.fun.trans<<','<<reg.val.fun.k <<')';
  }
  

  return (os<<""); 
}


// ***************** declaration
/*
typedef union _varVal
{
  boolForm bool;
  Transitions trans;
  region reg;  
} varVal;

ostream & operator <<(ostream & os, const varVal & varval);

typedef struct _declaration
{
  char* id;
  int type;
  varVal val;
} declaration;

#define BOOL 1
#define TRANS 2
#define REGION 3 

*/


declaration CreateDeclaration(const char* id, boolForm bf)
{
  declaration res;
  res.id=strdup(id);
  res.type=1;
  res.val.boolF=bf;
  return res;  
}

declaration CreateDeclaration(const char* id, Transitions tr)
{
  declaration res;
  res.id=strdup(id);
  res.type=2;
  res.val.trans=tr;
  return res;      
}

declaration CreateDeclaration(const char* id, region reg)
{
  declaration res;
  res.id=strdup(id);
  res.type=3;
  res.val.reg=reg;       
  return res;
}

ostream & operator <<(ostream & os, const declaration & decl)
{
  if (decl.type==1) return os<<"boolean "<<decl.id<<":="<< decl.val.boolF;
  else if (decl.type==2) return os<<"Transitions "<<decl.id<<":="<< decl.val.trans;
  else if (decl.type==3) return os<<"Region "<<decl.id<<":="<< decl.val.reg;
  else return os<<"ERREUR dans declaration";  
}


// ***************** if_then_else

/*
typedef struct _ifThenElse 
{
  boolForm cond;
  struct _instructionlist* If;
  struct _instructionlist* Else;
} ifThenElse;
*/

ifThenElse CreateIfThenElse (boolForm bf,   struct _instructionlist  If,   struct _instructionlist Else)
{
  ifThenElse res;
  res.cond=bf;

  res.If=( struct _instructionlist*)malloc(sizeof(struct _instructionlist));
  *(res.If)=If;

  res.Else=( struct _instructionlist*)malloc(sizeof(struct _instructionlist));
  *(res.Else)=Else;

  return res; 
}

ifThenElse CreateIfThenElse (boolForm bf,   struct _instructionlist  If)
{
  ifThenElse res;
  res.cond=bf;

  res.If=( struct _instructionlist*)malloc(sizeof(struct _instructionlist));
  *(res.If)=If;

  res.Else=NULL;
  
  return res;     
}

ostream & operator <<(ostream & os, const ifThenElse & ite)
{
  if (ite.Else==NULL) return os<<"IF "<<ite.cond<<endl<<"\tTHEN "<<*(ite.If)<<endl<<"ENDIF ";
  else return os<<"IF "<<ite.cond<<endl<<"\tTHEN "<<*(ite.If)<<endl<<"\tELSE "<<*(ite.Else)<<endl<<"ENDIF";   
}



// pour les instructions

/*
typedef union _instructionVal 
{
  declaration decl;
  procedure proc;  
  ifThenElse ite;  
} instructionVal;

typedef struct _instruction 
{
  int type;
  instructionVal val;
} instruction;

typedef struct _instructionlist 
{
  instruction i;  
  struct _instructionlist* next;  
} instructionlist;

#define DECLARATION 1
#define PROCEDURE 2
#define IFTHENELSE 3
*/

instruction CreateInstruction (declaration decl)
{
  instruction res;
  res.type=1;
  res.val.decl=decl;
  return res;  
}


instruction CreateInstruction (procedure proc)
{
  instruction res;
  res.type=2;
  res.val.proc=proc;
  return res;    
}


instruction CreateInstruction (ifThenElse ite)
{
  instruction res;
  res.type=3;
  res.val.ite=ite;
  return res;  
}


ostream & operator <<(ostream & os, const instruction & i)
{
  if (i.type==1) return os<<i.val.decl;
  else if (i.type==2) return os<<i.val.proc;
  else if (i.type==3) return os<<i.val.ite;
  else return os<<"erreur dans instruction, instruction numero "<<i.type<<" inconnue ";  
}

// ****************** instruction
/*
typedef struct _instructionlist 
{
  instruction i;  
  struct _instructionlist* next;  
} instructionlist;
*/

instructionlist CreateInstructionList (instruction i)
{
  instructionlist res;
  res.i =i;
  res.next=NULL; 
  return res;  
}


instructionlist operator+(const instructionlist & il, instruction i)
{
  instructionlist head=CreateInstructionList (i);
  head.next=(instructionlist*)malloc(sizeof(instructionlist));
  *(head.next)=il;
  return head;
}

instructionlist append(instructionlist head, instructionlist tail)
{
  instructionlist res=head;

  instructionlist current=head;
  while(current.next!=NULL) current=*(current.next);
  current.next=(instructionlist*)malloc(sizeof(instructionlist));
  *(current.next)=tail;
  
  return res;  
}

ostream & operator <<(ostream & os, const instructionlist & il)
{
  if (il.next!=NULL) return os<<il.i<<";"<<endl<<*(il.next);
  else  return os<<il.i<<";";  
}



// ****************** strategy

/*
typedef struct _strategy 
{
  char* name;
  instructionlist val;  
} strategy;
*/

strategy CreateStrategy(const char* id, instructionlist il)
{
  strategy res;
  res.name=strdup(id);
  res.val=il;
  return res;  
}


ostream & operator <<(ostream & os, const strategy & s)
{
  return os<<"strategy "<<s.name<<'{'<<endl<<s.val<<endl<<'}';  
}


// ****************** modelStrategy

/*
typedef struct _modelStrategy 
{
  model mod;
  strategy str;  
} modelStrategy;
*/

modelStrategy CreateModelStrategy(model mod, strategy str)
{
  modelStrategy res;
  res.mod=mod;
  res.strateg=str;
  return res;       
}

ostream & operator <<(ostream & os, const modelStrategy & s)
{
  return os<<s.mod<<endl<<endl<<s.strateg;  
}
