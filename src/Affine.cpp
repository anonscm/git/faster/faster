/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "Affine.h"
#include "LinearSum.h"
#include "Relation.h"

using namespace std;


Affine::Affine(const SquareMatrix & M, 
               const Vector & v, 
               const LNDD & guard, 
               const Order &order, 
               const string & name="")
  :order(order),guard(guard),M(M),v(v),name(name)
{
}



bool Affine::positive() const
{

      for(int i=1;i<=v.m;i++){  
        if (v(i,1)<0) return false;
      }
      
      return true;
}


Affine Affine::dc() const   // pour downwardclosure
{

  Affine res = Affine(M,v,guard,order);
      for(int i=1;i<=res.v.m;i++){  
        if (res.v(i,1)>0) res.v(i,1)=1;	
      }
      
       return res;
}



Relation Affine::toRelation() const 
{ 
  Order global=Relation::makeRelationOrder(order);
  LNDD r=LNDD::everything(global);
  
  
  for(int i=0;i<(int)order.size();i++){
    LinearSum l;
    for(int j=0;j<(int)order.size();j++)
      l=l+M(i+1,j+1)*LinearSum(order[j]);
    l=l+v(i+1,1)-LinearSum(PrimeVariables::prime(order[i]));
    r=r&LNDD(l,"==0",global);
  } 
  
  return Relation(r,order)&Relation::settounprimeRelation(guard);
}



// Compute a couple (R,D) of integers such that D>0, R>=0 and M^{R+D}=M^R. 
// Return R=0 and D=0 if such a couple does not exist.
void computeRD(const Matrix &M, int & R, int & D)
{
  //We just implement a weak test D up to 15. It is enough sufficient.
  int Dmax=15;
  R=M.n;
  D=1;
  Matrix Mr=M^R;
  Matrix Mrd=Mr*M;
  while ((Mr!=Mrd) && (D<Dmax)) {
    D++;
    Mrd=Mrd*M;
  }
  if (D==Dmax){
    R=0;
    D=0;
  }
  else {
    R=0;
    Mr=M^0;
    Mrd=M^D;
    while (Mr!=Mrd) {
      R++;
      Mr=Mr*M;
      Mrd=Mrd*M;
    }
  }
}



Relation Affine::convexStartoRelation() const // manquerait pas des free ??
{
  Affine newf = this->removeGuard();   // f without guard


  Relation r = newf.toRelation();
  LNDD d = this->guard;  //domain D
  LNDD d2= compose(r,this->guard); // domain D+v
  
  Relation r1 =(newf.startoRelation());
    
  r1=r1&Relation::settounprimeRelation(d);
  
  r1=r1&Relation::settoprimeRelation(d2);
  
  r1=r1|Relation::equality(d.order);
  
  Relation rel = r1;
  
  return rel;
}



Relation Affine::positiveStartoRelation() const  // garde close par le haut et vecteur positif !!
{
  Affine newf = this->removeGuard();   // f without guard

  LNDD d = this->guard;  //domain D
  Relation r1 =(newf.startoRelation());
  r1=r1&Relation::settounprimeRelation(d);
  r1=r1|Relation::equality(d.order);  
  return r1;
}



Relation Affine::startoRelation() const
{
  //On calcule s'=f^i(s);
  int r,delta;
  computeRD(M,r,delta);
  if (delta==0){
    //Fuction is not accelerable => just return the relation x'=f(x)/\x'=x  rather than f^*
    return 
      (toRelation() | Relation::equality(order));
  }
  int m=(int)order.size();
  string i="_i";
  
  Order global=Relation::makeRelationOrder(order);
  global.push_back(i);
  
  string t_k="_t";
  
  LNDD equ=LNDD::nothing(global);
 
  global=global.newVariableBack(t_k);
  // on ecrit l'automate associe a la partie cyclique
  for(int k=0; k<delta; k++){
    LNDD equ1(LinearSum(i)-delta*LinearSum(t_k)-k-r,"==0",global);
    Matrix A=M^(k+r);
    Matrix u=Matrix(m,1);
    u.fill(0);
    Matrix b=Matrix(m,1);
    b.fill(0);
    for(int j=0;j<k+r;j++)   u=u+(M^j)*v;
    for(int j=0;j<delta;j++) b=b+(M^(j+r+k))*v;
    // on a donc calcule A, u, b tel que s'=A.s+u+t_k.b    
    for(int j=1;j<=m;j++){
      LinearSum t;
      for(int l=1;l<=m;l++) {
        t=t+A(j,l)*LinearSum(order[l-1]);
      }
      equ1=
        equ1&
        LNDD(LinearSum(t_k),">=0",global)&
        LNDD(t + u(j,1)+b(j,1)*LinearSum(t_k) - LinearSum(PrimeVariables::prime(order[j-1])),"==0",global);
    }
    // on suprime la variable t_k=p_{2*m+1}
    equ1=equ1.getridof(t_k);
    
    equ=equ|equ1;
  }
  global=global.getridof(t_k);
  
  


  // On ecrit l'automate associe a la partie finie
  for(int k=0;k<r;k++){
    LNDD equ1(LinearSum(i)-k,"==0",global);
    
    Matrix A=M^k;
    Matrix u=Matrix(m,1);
    u.fill(0);
    for(int j=0;j<k;j++) u=u+(M^j)*v;
    // on a donc calcule A, u tel que s'=A.s+u    
    for(int j=1;j<=m;j++){
      LinearSum t;
      for(int l=1;l<=m;l++) {       
        t=t+A(j,l)*LinearSum(order[l-1]);
      }
      
      equ1=equ1&LNDD( t + u(j,1) - LinearSum(PrimeVariables::prime(order[j-1])),"==0", global);
    }
    
    equ=equ|equ1;
  }
  // equ := s'=f^i(s) 
  

  
  string k="_k";
  
  // On construit equ2= "f^i(s)\not\in\guard"
  LNDD equ2=equ&(Relation::settoprimeRelation(guard).newVariableBack(i));
  for(int j=0;j<m;j++)
    equ2=equ2.getridof(PrimeVariables::prime(order[j]));
  equ2=!equ2;
  
  equ2=equ2.newVariableBack(k);
  // On construit (0<=i<=k-1) & (f^i(s)\not\in\guard)
  equ2=equ2&
    LNDD( LinearSum(k)-LinearSum(i)-1,">=0",equ2.order)&
    LNDD( LinearSum(i),">=0",equ2.order);
  // \exist i : [(0<=i<=k-1) & (f^i(s)\not\in\guard)]
  equ2=equ2.getridof(i);
  // \forall i 0<=i<=k-1 \Rightarrow f^i(s)\in\guard
  equ2=!equ2;
  
  // On renome la variable k en i
  equ2.order[equ2.order.position(k)]=i;
  //p[i]="_i'";
  equ2=Relation::settounprimeRelation(equ2).getridof("_i'");
  
  // equ= " s'=f^i(s) "
  // equ= " (i>=0) & s'=f^i(s) & \forall k 0<=k<=i-1 \Rightarrow f^k(s)\in\guard "
  equ=LNDD(LinearSum(i),">=0",equ2.order)&equ&equ2;
  // \exist i>=0 s'=f^i(s) & \forall k 0<=k<=i-1 \Rightarrow f^k(s)\in\guard 
  equ=equ.getridof(i);
  return Relation(equ,order);
}







Affine compose(const Affine & f1, const Affine &f2)
{
  //A FAIRE:Verifier les order et prime.
  return Affine(f1.M*f2.M,
                f1.M*f2.v+f1.v,
                compose(f1.guard,f2.toRelation()), // first f2 then f1
                f1.order,
                f2.name+"."+f1.name);
}



Affine Affine::removeGuard(void) const
{
  return Affine(M,v,LNDD::everything(order),order,name);
}


bool operator==(const Affine &f1, const Affine &f2)
{
  //IL FAUDRAIT TESTER order et prime
  return (f1.M==f2.M)&&(f1.v==f2.v)&&(f1.guard==f2.guard);
}

Affine Affine::identity(const Order &order)
{
  return Affine(SquareMatrix::identity(order),
                Vector::zero(order),
                LNDD::everything(order),
                order,
                "Id");
}

ostream & operator<<(ostream & os, const Affine &f)
{
  os<<"Name="<<f.name<<endl
    <<"M="<<endl<<f.M
    <<"v="<<endl<<f.v
    //    <<"Guard="<<endl<<f.guard
    <<"Guard="<<endl<<" work in progress :) "<<endl
    <<"Order="<<f.order;
  return os;
}
