/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef ARBSYNTOFAST_H
#define ARBSYNTOFAST_H

#include <cstdio>
#include <iostream>  
//#include <malloc.h>
#include <cstring>
#include <cstdlib>
#include "Strategy.h"
#include <string>

class Strategy;

extern int MAX_STATES;
extern int MAX_ACCELERATIONS;


string unprimeString (string s);

LinearSum linearsumTreeToLinearSum (linearsumTree t);

LNDD pbgTermToLNDD (pbgTerm pbgt, const Order & o);

LNDD pbgFormToLNDD (pbgForm form,  const Order & o, map<string,int>  stateval);

SquareMatrix actionToMatrix (action act,  const Order & order, SquareMatrix m);

Vector actionToVector (action act,  const Order & o, Vector v);

SquareMatrix actionListToMatrix (actionlist actlist,  const Order & order);

Vector actionListToVector (actionlist actlist,  const Order & order);

Affine transToAffine  (transition trans, const Order & order, const PrimeVariables & prime, map<string,int>  stateval);

LinearSystem transListToLSys  (transitionlist tl, const Order & order, const PrimeVariables & prime, map<string,int>  stateval, string name);

LinearSystem modelToLSys (model mod);

LNDD regionToLNDD (region reg, map<string, LNDD> memory, map<string, LinearSystem> memoryTrans,  const Order & o,  const PrimeVariables & prime,  LinearSystem s, map<string,int>  stateval);

bool boolFormToBool (boolForm bf, map<string, bool> memory, map<string, LNDD> memReg, map<string, LinearSystem> memTrans, const Order & o,  const PrimeVariables & prime,  LinearSystem s, map<string,int>  stateval) ;

LinearSystem TransitionsToLinearSystem (Transitions trans, map<string, LinearSystem> memory, const Order & o, const PrimeVariables & prime, LinearSystem s);

Strategy modelStrategyToStrategy (modelStrategy modStra);

#endif
