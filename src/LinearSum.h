/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef LINEARSUM_H
#define LINEARSUM_H



using namespace std;

#include <iostream>
#include <map>
#include <string>

class LinearSum
{



 private:
  //typedef map<string,int> MapIntInt;
  //MapIntInt iimap;
  int constante;
 public:
typedef map<string,int> MapIntInt;
  MapIntInt iimap;
  LinearSum();                  // definit la somme ""
  LinearSum(int i);             // definit la somme "i"
  LinearSum(const string &name); // definit la somme "Variable(v)"
  
  LinearSum simplify() const;
  int getValue(const string &name) const;
  int getConstante(void) const;
  
  // multiplication par un entier seulement
  friend LinearSum operator*(const LinearSum &l, int c);
  friend LinearSum operator*(int c, const LinearSum &l);
  
  // addition et soustration binaine et soustraction unaire.
  friend LinearSum operator+(const LinearSum &l1, const LinearSum &l2);
  friend LinearSum operator-(const LinearSum &l1, const LinearSum &l2);
  friend LinearSum operator-(const LinearSum &l1);

  friend ostream & operator <<(ostream & os, const LinearSum &l);


};


#endif
