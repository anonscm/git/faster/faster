/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "LinearSum.h"

// INITIALISATION
LinearSum::LinearSum():constante(0) 
{  
}

LinearSum::LinearSum(int i):constante(i) 
{  
}

LinearSum::LinearSum(const string &name)
{  
  iimap[name]=1;
  constante=0;
}


LinearSum LinearSum::simplify() const
{
  LinearSum linearSum;
  linearSum.constante=constante;
  for(LinearSum::MapIntInt::const_iterator i=iimap.begin();i!=iimap.end();i++) 
    if (i->second!=0)
      (linearSum.iimap)[i->first]=i->second;
  return linearSum;
}



// multiplication par un entier seulement
LinearSum operator*(const LinearSum &l, int c)
{
  if (c==0)
    return LinearSum();
  LinearSum linearSum=l;
  linearSum.constante*=c;
  for(LinearSum::MapIntInt::iterator i=linearSum.iimap.begin();i!=linearSum.iimap.end();i++)
    i->second*=c;
  return linearSum;
}
LinearSum operator*(int c, const LinearSum &l)
{
  return l*c;
}

int LinearSum::getConstante(void) const
{
  return constante;
}
int LinearSum::getValue(const string &name) const
{
  LinearSum::MapIntInt::const_iterator pos=iimap.find(name);
  if (pos==iimap.end())
    return 0;
  else
    return pos->second;
}




// addition et soustration binaine et soustraction unaire.
LinearSum operator+(const LinearSum &l1, const LinearSum &l2)
{
  LinearSum linearSum=l1;
  linearSum.constante+=l2.constante;
  for(LinearSum::MapIntInt::const_iterator i=l2.iimap.begin();i!=l2.iimap.end();i++) {
    LinearSum::MapIntInt::iterator pos=linearSum.iimap.find(i->first);
    if (pos==linearSum.iimap.end())
      (linearSum.iimap)[i->first]=i->second;      
    else
      pos->second+=i->second;
  }
  return linearSum.simplify();
}
LinearSum operator-(const LinearSum &l)
{
  return l*(-1);
}
LinearSum operator-(const LinearSum &l1, const LinearSum &l2)
{
  return l1+(-l2);
}

ostream & operator <<(ostream & os, const LinearSum &l)
{
  for(LinearSum::MapIntInt::const_iterator i=l.iimap.begin();i!=l.iimap.end();i++)
    os<<i->second<<"*"<<i->first<<" + ";
  os<<l.constante<<endl;
  return os;
}
