/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "LNDD.h"

#include <vector>
#include <string>
#include <iostream>

genepi_solver *LNDD::GENEPI_SOLVER = NULL;

genepi_set *
LNDD::CreateEq(const LinearSum &lsum, const Order &ord)
{
  int m = (int)(ord.size());
  int position = 0;  
  int *val = (int *)calloc (sizeof (int), m);
  // op =="==0"
  
  for(int i=0; i<m;i++){
    val[i]=0;    
  }
  
  for(LinearSum::MapIntInt::const_iterator i=lsum.iimap.begin();
      i!=lsum.iimap.end();i++){    
    position = ord.position(i->first);
    val[position]=i->second;
  }
  
  genepi_set* set1 = genepi_set_linear_operation(GENEPI_SOLVER,val,m,
						 GENEPI_EQ,-lsum.getConstante());    
  genepi_set* set2 = genepi_set_top_N (GENEPI_SOLVER, m);
  genepi_set* result=genepi_set_intersection(GENEPI_SOLVER,set1,set2);
  genepi_set_del_reference(GENEPI_SOLVER,set1);
  genepi_set_del_reference(GENEPI_SOLVER,set2);
  free (val);

  return(result);     
}

genepi_set *
LNDD::CreateInEq(const LinearSum &lsum, const Order &ord)
{
  int m = (int)(ord.size());
  int position = 0;  
  int *val = (int *) calloc(sizeof (int), m);
  // op ==">=0"
  
  for(int i=0; i<m;i++){
    val[i]=0;    
  }
  
  for(LinearSum::MapIntInt::const_iterator i=lsum.iimap.begin();i!=lsum.iimap.end();i++){    
    position = ord.position(i->first);
    val[position]=i->second;
  }
  
  genepi_set* set1 = 
    genepi_set_linear_operation(GENEPI_SOLVER,val,m, GENEPI_GEQ,
				-lsum.getConstante()); 
  genepi_set* set2 = genepi_set_top_N (GENEPI_SOLVER, m);
  genepi_set* result=genepi_set_intersection(GENEPI_SOLVER,set1,set2);
  genepi_set_del_reference(GENEPI_SOLVER,set1);
  genepi_set_del_reference(GENEPI_SOLVER,set2);
  free (val);

  return(result);     
}


LNDD::LNDD(const LinearSum &lsum, const string &op, const Order &ord)
{ 
  order=Order(ord);    
  if (op=="==0"){a = CreateEq(lsum,ord);} 
    else a = CreateInEq(lsum,ord);  
}




LNDD::LNDD(const LNDD & lndd)
{
  order=lndd.order;  
  a=genepi_set_add_reference(GENEPI_SOLVER,lndd.a);
}


LNDD & LNDD::operator=(const LNDD & lndd)
{
  order=lndd.order;
  if (this!=&lndd) {
    genepi_set_del_reference(GENEPI_SOLVER,a);
    a=genepi_set_add_reference(GENEPI_SOLVER,lndd.a);
  }
  return *this;  
}

LNDD::~LNDD(void)
{
  genepi_set_del_reference(GENEPI_SOLVER,a);
}

LNDD LNDD::operator&(const LNDD &lndd) const
{
  //the order must be the same... No test is done but...
  LNDD l1 =  LNDD(genepi_set_intersection(GENEPI_SOLVER,a,lndd.a),order);  
  return l1;  
}

LNDD LNDD::operator|(const LNDD &lndd) const
{
  LNDD l1 =   LNDD(genepi_set_union(GENEPI_SOLVER,a,lndd.a),order);  
  return l1; 
}


LNDD LNDD::operator-(const LNDD &lndd) const
{
  return (*this)&(!lndd);
}



LNDD LNDD::operator!() const
{
  int size=(int)(order.size());
  genepi_set *set1=genepi_set_complement(GENEPI_SOLVER,a);
  genepi_set *set2=genepi_set_top_N (GENEPI_SOLVER, size);
  genepi_set *result=genepi_set_intersection(GENEPI_SOLVER,set1,set2);
  genepi_set_del_reference(GENEPI_SOLVER,set1);
  genepi_set_del_reference(GENEPI_SOLVER,set2);
  return LNDD(result,order);  
}


LNDD::LNDD(genepi_set *da, const Order &ord)
{
  a=da;// ATTENTION, faut copier le genepi_set avant !!! 
  order= Order(ord);
  
}

bool LNDD::isEmpty(void) const
{
  return genepi_set_is_empty(GENEPI_SOLVER,a);  
}


LNDD LNDD::everything(const Order &order)
{  
  int size=(int)(order.size());
  return LNDD(genepi_set_top_N (GENEPI_SOLVER, size),order);
}

LNDD LNDD::nothing(const Order &order)
{
  int size=(int)(order.size());
  return LNDD(genepi_set_bot(GENEPI_SOLVER,size),order);
}




int LNDD::size() const 
{
  return genepi_set_get_data_structure_size(GENEPI_SOLVER,a);
}


LNDD::LNDD(void)
{

  a= genepi_set_bot(GENEPI_SOLVER,1); // un peu crade, mais est utilise juste pour declarer
  order=Order();
}



LNDD LNDD::getridof(const string & name) const
{
  int position = order.position(name);
  int size=(int)(order.size());
  int *selection = (int *) calloc(sizeof (int), size);
   // on remplit le tableau pour la projection
   for (int i=0; i<size;i++){
     if  (i == position) selection[i]=1;
     else selection[i]=0;      
   }    
   // fin du remplissage  
  
   genepi_set* a1 = genepi_set_project(GENEPI_SOLVER,a,selection,size);
   free (selection);

   return LNDD(a1,order.getridof(name));
}

LNDD LNDD::exist(const string & ident, const LNDD & lndd )
{
  return lndd.getridof(ident);
}

LNDD LNDD::newVariableBack(const string & name) const
{
    Order ord2 =  Order(order);    
    ord2.push_back(name);
    int size = (int)(ord2.size());    
    int *selection = (int *) calloc(sizeof (int), size);
			  
    // on remplit le tableau pour la projection
    for (int i=0; i<size-1;i++){
      selection[i]=0;      
    }    
    selection[size-1]=1;
    // fin du remplissage    
    genepi_set* set1 = genepi_set_invproject(GENEPI_SOLVER,a,selection,size);
    genepi_set* set2 = genepi_set_top_N (GENEPI_SOLVER, size);
    genepi_set* result=genepi_set_intersection(GENEPI_SOLVER,set1,set2);
    genepi_set_del_reference(GENEPI_SOLVER,set1);
    genepi_set_del_reference(GENEPI_SOLVER,set2);  
    free (selection);

    return LNDD(result,ord2);        
}


bool LNDD::operator<=(const LNDD &lndd) const  
{
  return genepi_set_compare(GENEPI_SOLVER,a,GENEPI_LEQ,lndd.a);
}


bool LNDD::operator>=(const LNDD &lndd) const
{
  return genepi_set_compare(GENEPI_SOLVER,a,GENEPI_GEQ,lndd.a);
}


bool LNDD::operator==(const LNDD &lndd) const  
{
  return genepi_set_compare(GENEPI_SOLVER,a, GENEPI_EQ, lndd.a);
}


void LNDD::setGenepiSolver (genepi_solver *solver)
{
  GENEPI_SOLVER = solver;
}

			/* --------------- */

void LNDD::displayAllElements(FILE *output) const
{
  int i;
  const char **varnames = new const char *[order.size()];

  for(i = 0; i < order.size (); i++) {
    varnames[i] = order[i].c_str ();
  }
  genepi_set_display_all_solutions (GENEPI_SOLVER, a, output, varnames);
  delete[] varnames;
}
