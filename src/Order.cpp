/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "Order.h"

int Order::position(const string &name) const
{
  for(int i=0;i<(int)size();i++)
    if (operator[](i)==name)
      return i;
  return (int)size();
}

Order Order::getridof(const string & name) const
{
  Order neworder=(*this);
  int i=position(name);
  if (i<(int)neworder.size()) {
    for(int j=i+1;j<(int)neworder.size();j++)
      neworder[j-1]=neworder[j];
    neworder.pop_back();
  }
  return neworder;
}

Order Order::getridof(const stringSet & nameSet) const
{
  Order neworder=(*this);
  for(int i=0;i<(int)nameSet.size();i++)
    neworder=neworder.getridof(operator[](i));
  return neworder;
}

Order Order::newVariableBack(const string& name) const
{
  Order neworder=(*this);
  neworder.push_back(name);
  return neworder;
}

ostream & operator<<(ostream & os, const Order & order)
{
  for(int i=0;i<(int)order.size();i++)
    os<<order[i]<<", ";
  os<<endl;
  return os;
}


