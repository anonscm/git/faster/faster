/*
 * fast.cpp -- Main program of the FAST tool
 * Copyright (C) 2003-2007 Jerome Leroux, Sebastien Bardin, Alain Finkel, 
 * Gerald Point and LSV,CNRS UMR 8643 & ENS Cachan, LaBRI, CNRS UMR 5800, 
 * Universite Bordeaux I
 *
 * FAST, an accelerated symbolic model-checker. 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/* $Id: fast.cpp,v 1.19 2009/04/23 15:42:01 point Exp $ */

/**
 * @author  : $Author: point $
 * @version : $Revision: 1.19 $
 * @date    : $Date: 2009/04/23 15:42:01 $
 */
#include <stdlib.h>
#include <assert.h>
#include <genepi/genepi.h>
#include <genepi/genepi-loader.h>
#include "fast.h"

#define DEFAULT_CACHE_ENTRIES 50000
#define DEFAULT_CACHE_SIZE 0

#ifndef GENEPI_LOG_FILE 
# define GENEPI_LOG_FILE "genepi_tracefile.dijo"
#endif /* ! GENEPI_LOG_FILE */

#define ENV_FAST_DEFAULT_ENGINE "FAST_DEFAULT_ENGINE"
extern FILE *yyin;

			/* --------------- */

static void
s_usage (char *cmd);

static void
s_load_plugins (genepi_plugin **pplugin);

static int 
s_is_long_opt (const char *arg, const char *opt, const char **parg);

static unsigned int
s_atoui(const char *s);

			/* --------------- */

int
main (int argc, char **argv)
{
  int structure = 0;
  int result = 0;
  const char *filename = NULL;
  int flags = 0;
  int autotest = 0;
  unsigned int seed = (unsigned int) time (NULL);
  genepi_plugin *plugin = NULL;
  genepi_solver *solver = NULL;
  FILE *trace_file = NULL;
  unsigned int cachesize = 0;
  unsigned int nbentries = DEFAULT_CACHE_ENTRIES;

  genepi_loader_init ();
  s_load_plugins (&plugin);

  for (int i = 1; i < argc; i++)
    {
      char *arg = argv[i];

      if (*arg == '-')
	{
	  arg++;

	  if (strcmp (arg, "q") == 0)
	    VERBOSE = 0;
	  else if (strcmp (arg, "h") == 0)
	    {
	      s_usage (argv[0]);
	      genepi_loader_terminate ();
	      exit (1);
	    }
	  else if (strcmp (arg, "t") == 0)
	    flags |= GENEPI_FLAG_SHOW_TIMERS;
	  else if (*arg == '-')
	    {
	      const char *opt_arg = NULL;
	      arg++;
	      if (strcmp (arg, "auto") == 0)
		autotest = 1;
	      else if (strcmp (arg, "structure") == 0)
		structure = 1;
	      else if (strcmp (arg, "show-cache-stats") == 0)
		flags |= GENEPI_FLAG_SHOW_CACHE_INFO;
	      else if (s_is_long_opt(arg, "trace-genepi",&opt_arg))
		{
		  if (opt_arg == NULL)
		    opt_arg = GENEPI_LOG_FILE;
		  
		  trace_file = fopen (opt_arg, "w");
		  if (trace_file == NULL)
		    {
		      fprintf (stderr, "can't open trace file '%s'.\n",
			       opt_arg);
		      result = 1;
		      goto end;
		    }
		  else
		    {
		      printf ("output GENEPI traces into: %s\n", opt_arg);
		    }
		}
	      else if (s_is_long_opt (arg, "cache-size", &opt_arg))
		{
		  if (opt_arg == NULL)
		    {
		      fprintf (stderr, "error: missing argument to option "
			       "'%s'.\n",arg);
		      result = 1;
		      goto end;
		    }
		  else
		    {
		      cachesize = s_atoui(opt_arg);
		    }
		}
	      else if (s_is_long_opt (arg, "number-of-entries", &opt_arg))
		{
		  if (opt_arg == NULL)
		    {
		      fprintf (stderr, "error: missing argument to option "
			       "'%s'.\n",arg);
		      result = 1;
		      goto end;
		    }
		  else
		    {
		      nbentries = s_atoui(opt_arg);
		    }
		}
	      else if (strcmp (arg, "help") == 0)
		{
		  s_usage (argv[0]);
		  if (plugin != NULL)
		    genepi_plugin_del_reference (plugin);
		  genepi_loader_terminate ();
		  exit (1);
		}
	      else if (s_is_long_opt (arg, "seed", &opt_arg))
		{
		  if (strlen (opt_arg) == 0)
		    {
		      fprintf (stderr, "error: missing argument to option "
			       "'%s'.\n",arg);
		      result = 1;
		      goto end;
		    }
		  else if ('0' <= *opt_arg && *opt_arg <= '9')
		    seed = s_atoui (opt_arg);
		  else
		    {
		      FILE *rndfile = fopen (opt_arg,"r");
		      if (rndfile == NULL)
			{
			  fprintf (stderr, "error: can't open random file "
				   "'%s'.\n",opt_arg);
			  result = 1;
			  goto end;
			}
		      if (!fread (&seed, sizeof(seed), 1, rndfile))
			{
			  fprintf (stderr, "error: can't read random file "
				   "'%s'.\n",opt_arg);
			  result = 1;
			  goto end;
			}
		      fclose (rndfile);
		    }
		}
	      else if (s_is_long_opt (arg, "engine", &opt_arg) ||
		       s_is_long_opt (arg, "load", &opt_arg) ||
		       s_is_long_opt (arg, "plugin", &opt_arg))
		{
		  if (opt_arg == NULL)
		    {
		      fprintf (stderr, "error: missing argument to option "
			       "'%s'.\n",arg);
		      result = 1;
		      goto end;
		    }

		  if (strncmp (arg, "engine=", strlen ("engine=")) == 0 ||
		      strncmp (arg, "load=", strlen ("load=")) == 0)
		    {
		      fprintf (stderr, "warning: options --engine and --load "
			       "are deprecated; use --plugin instead.\n");
		    }
		  if (plugin != NULL)
		    genepi_plugin_del_reference (plugin);
		  plugin = genepi_loader_get_plugin (opt_arg);
		  if (plugin == NULL)
		    cerr << "unknown engine '" << opt_arg << "'." << endl;
		}
	      else
		{
		  cerr << "unknown option '" << arg << "'." << endl;
		}
	    }
	  else
	    {
	      cerr << "unknown option '" << arg << "'." << endl;
	    }

	}
      else
	{
	  filename = arg;
	}
    }

  if (plugin == NULL)
    {
      cerr << "no engine is selected and the environment variable "
	<< ENV_FAST_DEFAULT_ENGINE << " is not\nset.\n" << endl;
      result = 1;
      goto end;
    }
  else
    {
      const char *name; 

      solver = genepi_solver_create (plugin, flags, cachesize, nbentries);
      genepi_plugin_del_reference (plugin);
      name = genepi_solver_get_name (solver);
      assert (name != NULL);

      if (trace_file != NULL)
	genepi_solver_set_trace_file (solver, trace_file);
      LNDD::setGenepiSolver (solver);

      if (VERBOSE)
	  cerr << "current set encoding is : " << name << endl;
    }

  if (VERBOSE)
      cerr << "seed is : " << seed << endl;
  srand (seed);

  if (autotest)
    {
      cout << "starting auto test" << endl;
      cout << " -- > auto test ";
      if (!(autotest = genepi_solver_run_autotest (solver)))
	cout << "failed";
      else
	cout << "passed";
      cout << " !" << endl
	   << "auto test done." << endl;
      if (!autotest || filename == NULL)
	{
	  result = (!autotest ? 1 : 0);
	  goto end;
	}
    }

  if (filename == NULL)
    {
      filename = "standard input";
      yyin = stdin;
    }
  else if ((yyin = fopen (filename, "r")) == NULL)
    {
      cerr << "error opening " << filename << "." << endl;
      goto end;
    }

  while (!feof (yyin))
    if (yyparse () != 0)
      break;

  if (isparseerror ())
    {
      cerr << "............. ABORTION ............ " << endl;
      result = 1;
      goto end;
    }

  // LNDD::initPositiveCache (0);
  {
    modelStrategy md = parserresult ();

    Strategy strateg = modelStrategyToStrategy (md);


    strateg.start ();
    if (structure)    
      {
	cout<<"//Display data-structure of the computed regions"<<endl;
	map_string_LNDD::iterator p;
	for(p =strateg.varreg.begin();
	    p!=strateg.varreg.end();
	    p++)
	  {
	    cout<<"//Structure of region "<<p->first<<endl;
	    genepi_set_display_data_structure(LNDD::GENEPI_SOLVER, (p->second).a, stdout);	
	    cout<<endl;
	  }
      }
    
  }
  // LNDD::terminatePositiveCache ();

 end:
  if (solver != NULL)
    genepi_solver_delete (solver);
  genepi_loader_terminate ();
  if (trace_file != NULL)
    {
      fflush (trace_file);
      fclose (trace_file);
    }

  if (result == 0 && VERBOSE)
    cerr << "seed is : " << seed << endl;

  return result;
}

			/* --------------- */

static void
s_usage (char *cmd)
{
  int nb_plugins = 0;
  char **plugins = genepi_loader_get_plugins (&nb_plugins);

  cerr << "USAGE: " << cmd
       << " [--help|-h] [--seed=n] [--auto] [-t] [--plugin=e] "
       << "[--trace-genepi] [--structure] [filename]" << endl
       << "with:" << endl
       << "  -h or --help display this help message." << endl
       << "  --seed=n set the seed of the pseudo-random number generator to "
       << endl
       << "           the integer 'n'." << endl
       << "  --auto execute some basic tests on the selected engine" << endl
       << "  --t display the total execution times of the API funcions" << endl
       << "  --plugin=p specify the engine used to encode tuple sets. e might "
       << endl
       << "             be the name of a registered plugin or the path to the "
       << endl
       << "             plugin .so file." << endl
       << "  --trace-genepi[=filename] generate all GENEPI operations into '"
       << GENEPI_LOG_FILE << "' file or filename is supplied." << endl
       << "  --structure display the data-structure of the computed regions "
       << endl
       << "  --cache-size=size-in-bytes sets the size of the GENEPI cache." 
       << " cache. default=" << DEFAULT_CACHE_SIZE << endl
       << "  --number-of-entries=integer sets the number of entries used by"
       << " the GENEPI cache. default = " << DEFAULT_CACHE_ENTRIES << endl
       << "  --show-cache-stats display informations about the GENEPI cache "
       << " usage." << endl
       << endl 
       << endl;

  if (nb_plugins == 0)
    cerr << "FASTer can't find any GENEPI plugin.\n" << endl;
  else
    {
      cerr << "Existing plugins are: ";
      for (int i = 0; i < nb_plugins; i++)
	{
	  cerr << plugins[i];
	  free (plugins[i]);
	  if (i != nb_plugins - 1)
	    cerr << ", ";
	}
      cerr << endl;
    }
}


			/* --------------- */

static void
s_load_plugins (genepi_plugin **pplugin)
{
  const char *default_plugin_name = getenv (ENV_FAST_DEFAULT_ENGINE);

  genepi_loader_load_default_plugins ();

  if (default_plugin_name != NULL)
    {
      *pplugin = genepi_loader_get_plugin (default_plugin_name);
      if (*pplugin == NULL)
	{
	  cerr << "unknown (default) plugin name '" << default_plugin_name
	    << "'." << endl;
	}
    }
}

			/* --------------- */

static int 
s_is_long_opt (const char *arg, const char *opt, const char **parg)
{
  int len = strlen (opt);

  if (parg != NULL) 
    *parg = NULL;

  if (strncmp (arg, opt, len) != 0)
    return 0;

  if (arg[len] == '=' && parg != NULL)
    *parg = arg+len+1;

  return 1;
}

			/* --------------- */

static unsigned int
s_atoui(const char *s)
{
  unsigned int result = 0;

  for(; *s; s++)
    result = 10*result+(unsigned int)(*s-'0');

  return result;
}
