/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef RELATION_H
#define RELATION_H
using namespace std;
#include "LNDD.h"
#include "PrimeVariables.h"

class Relation : public LNDD
{
 public:
  using LNDD::a;
  using LNDD::order;
  Order unprime;
  //Pour transformer un LNDD en une relation binaire.
  Relation(const LNDD &lndd, const Order &order);
  
  static Order makeRelationOrder(const Order &order);
  static Relation everything(const Order &order);
  static Relation nothing(const Order &order);
  static Relation equality(const Order &order);
  static Relation leq(const Order &order);
  Relation operator&(const Relation &relation) const;
  Relation operator|(const Relation &relation) const;
  Relation operator-(const Relation &relation) const;
  Relation operator!(void) const;
    static Relation settounprimeRelation(const LNDD &lndd);
    static Relation settoprimeRelation(const LNDD &lndd);
    //static Relation equality(const Order &order, const PrimeVariables &prime);
    LNDD getridofprime() const;
    LNDD getridofunprime() const;
 public:
  //  Relation(ndd *a, const Order &order, const Order &unprime, const PrimeVariables &prime);  
  
  

};

Relation compose(const Relation &relation1, const Relation &relation2);
LNDD     compose(const Relation &relation , const LNDD     &set);
LNDD     compose(const LNDD     &set      , const Relation &relation);
#endif
