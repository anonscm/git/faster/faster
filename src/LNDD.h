/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef LNDD_H
#define LNDD_H

//extern "C" {
//#include "dfa.h"
//}
#include <stdio.h>
#include <genepi/genepi.h>

#include "Order.h"
#include <string>
#include "LinearSum.h"
#include "stringSet.h"


class LNDD
{
 public:
  Order order;
  genepi_set *a;
 public:


  
  LNDD(const LinearSum &lsum, const string &op, const Order &order);
  LNDD(const LNDD & lndd);
  LNDD & operator=(const LNDD & lndd);
  ~LNDD(void);

  LNDD getridof(const string & name) const;
  static LNDD exist(const string & name, const LNDD & lndd );

  LNDD newVariableBack(const string & name) const;

  LNDD operator&(const LNDD &lndd) const;
  LNDD operator|(const LNDD &lndd) const;
  LNDD operator-(const LNDD &lndd) const;
  LNDD operator!() const;
  bool operator<=(const LNDD &lndd) const;
  bool operator>=(const LNDD &lndd) const;  
  bool operator==(const LNDD &lndd) const;  
  int size() const;

  void displayAllElements(FILE *output = stdout) const;
  

  static void setGenepiSolver (genepi_solver *solver);
  //static void initPositiveCache (int cache_size);
  //static void terminatePositiveCache ();

  static LNDD everything(const Order &order);
  static LNDD nothing(const Order &order);
  //static LNDD positive(const Order &order);
  //static LNDD DownwardClosure(LNDD &states, PrimeVariables &prime)  

  bool isEmpty(void) const;
  
 public:
  LNDD(void);
  LNDD(genepi_set *a,const Order &order);
  static genepi_solver *GENEPI_SOLVER;

 private:
  //static int positive_cache_size;
  //static bool *positive_cache_genepi_set;
  //static genepi_set **positive_genepi_set;

  //static genepi_set *positive(int alpha_size);
  static genepi_set *CreateEq(const LinearSum &lsum, const Order &ord);
  static genepi_set *CreateInEq(const LinearSum &lsum, const Order &ord);
};

//genepi_set*  CreateInEq(const LinearSum &lsum, const Order &ord);
// genepi_set*  CreateEq(const LinearSum &lsum, const Order &ord);




#endif
