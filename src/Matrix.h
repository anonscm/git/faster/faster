/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef MATRIX_H
#define MATRIX_H
#include "Order.h"

class Matrix
{

 public:
  int n;
  int m;
 private:
  int* valeur  ;
 public:
  Matrix();
  Matrix(int m, int n);
  Matrix(const Matrix & M);
  Matrix & fill(const int x);
  Matrix & operator=(const Matrix & M);
  bool operator==(const Matrix & M) const;
  bool operator!=(const Matrix & M) const;
  ~Matrix();
  int & operator()(int i, int j) const;
  static Matrix identity(const int n);
};

Matrix operator *(const int x, const Matrix & A);  
Matrix operator +(const Matrix & A, const Matrix & B);  
Matrix operator -(const Matrix & A, const Matrix & B);  
Matrix operator -(const Matrix & A);
Matrix operator *(const Matrix & A, const Matrix & B);
Matrix operator ^(const Matrix & M, const int i);
bool   operator <(const Matrix & A, const Matrix & B);
ostream & operator <<(ostream & os, const Matrix & M);


class SquareMatrix : public Matrix
{
 public:
  static SquareMatrix identity(const Order & order);
  static SquareMatrix transfer(const string & tostate, const string & fromstate, const Order & order);
  SquareMatrix(const Matrix &M):Matrix(M)
    {
    }
  
};


class Vector : public Matrix
{
 public:
  Vector(const string &name, const Order &order);
  static Vector zero(const Order &order);
  Vector(const Matrix &M):Matrix(M)
    {
    }
};
    
#endif
