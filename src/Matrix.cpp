/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include "Matrix.h"
#include <iostream>

Matrix::Matrix()
{
  n=0;
  m=0;
  valeur=new int[0];   
}

Matrix::Matrix(int m, int n) 
{
  valeur=new int[m*n];

  this->n=n;
  this->m=m;
  for(int i=1;i<=m;i++)
    for(int j=1;j<=n;j++)
      (*this)(i,j)=0;
}

Matrix::~Matrix()
{
  delete[] valeur;
}


Matrix::Matrix(const Matrix & M)
{
  int i,j;
  this->n=M.n;
  this->m=M.m;
  valeur=new int[m*n];
  for(i=1;i<=M.m;i++) for(j=1;j<=M.n;j++)
    (*this)(i,j)=M(i,j);
}


Matrix & Matrix::operator=(const Matrix & M) 
{
  int i,j;
  if (this!=&M) {
    delete[] valeur;
    this->n=M.n;
    this->m=M.m;
    valeur=new int[m*n];
    for(i=1;i<=M.m;i++) for(j=1;j<=M.n;j++)
      (*this)(i,j)=M(i,j);
  }
  return *this;
}

bool Matrix::operator==(const Matrix & M) const
{
  int i,j;
  if ((m!=M.m)||(n!=M.n)) return false;
  for (i=1;i<=m;i++) for (j=1;j<=n;j++)
    if ((*this)(i,j)!=M(i,j))
      return false;
  return true;
}
bool Matrix::operator!=(const Matrix & M) const
{
  return !(*this==M);
}

Matrix operator *(const Matrix & A, const Matrix & B)
{
  Matrix M=Matrix(A.m,B.n);
  int i,j,k;
  int val;
  int d;
  d=A.n;
  for(i=1;i<=M.m;i++) for(j=1;j<=M.n;j++) {
    val=0;
    for(k=1;k<=d;k++) val+=A(i,k)*B(k,j);
    M(i,j)=val;
  }
  return M;
}

Matrix operator +(const Matrix & A, const Matrix & B)
{
  Matrix M=Matrix(A.m,A.n);
  for(int i=1;i<=M.m;i++) 
    for(int j=1;j<=M.n;j++)
      M(i,j)=A(i,j)+B(i,j);
  return M;
}

Matrix operator -(const Matrix & A, const Matrix & B)
{
  Matrix M=Matrix(A.m,A.n);
  for(int i=1;i<=M.m;i++) 
    for(int j=1;j<=M.n;j++)
      M(i,j)=A(i,j)-B(i,j);
  return M;
}

Matrix operator -(const Matrix & A)
{
  Matrix M=Matrix(A.m,A.n);
  for(int i=1;i<=M.m;i++) 
    for(int j=1;j<=M.n;j++)
      M(i,j)=-A(i,j);
  return M;
}

Matrix operator *(const int x, const Matrix & A)
{
  Matrix M=Matrix(A.m,A.n);
  int i,j;
  for(i=1;i<=M.m;i++) for(j=1;j<=M.n;j++) 
    M(i,j)=x*A(i,j);
  return M;
}
 

Matrix & Matrix::fill(const int x)
{
  int i,j;
  for(i=1;i<=m;i++) for(j=1;j<=n;j++)
    (*this)(i,j)=x;
  return *this;
}

Matrix Matrix::identity(const int n) 
{
  Matrix M=Matrix(n,n);
  int i,j;
  for(i=1;i<=n;i++) for(j=1;j<=n;j++)
    if (i==j) 
      M(i,j)=1;
    else
      M(i,j)=0;
  return M;
}




Matrix operator ^(const Matrix & M, const int k)
{
  int n=k;
  Matrix H=M;
  Matrix P=Matrix::identity(M.m);
  while (n!=0) {
    if (n%2==1) P=P*H;
    H=H*H;
    n=n/2;
  }
  return P;
}


int & Matrix::operator()(int i, int j) const
{
  return valeur[(i-1)*n+(j-1)];
}

bool operator<(const Matrix & A, const Matrix & B)
{
  if (A.m<B.m) return true; 
  else if (A.m>B.m) return false;
  if (A.n<B.n) return true; 
  else if (A.n>B.n) return false;
  //A.m==B.m et A.n==B.n
  int m=A.m;
  int n=A.n;
  for(int i=1;i<=m;i++) for(int j=1;j<=n;j++)
    if (A(i,j)<B(i,j)) return true; 
    else if (A(i,j)>B(i,j)) return false;
  return false;
}

ostream & operator<<(ostream & os, const Matrix & M) 
{
  int i,j;
  for(i=1;i<=M.m;i++) {
    os << "[";    
    for(j=1;j<=M.n;j++)
      os <<  M(i,j)<<", ";
    os << "]"<< endl;
  }
  return os;
}
















SquareMatrix SquareMatrix::identity(const Order & order)
{
  return Matrix::identity(order.size());
}

SquareMatrix SquareMatrix::transfer(const string & tostate, const string & fromstate, const Order & order)
{
  Matrix M=Matrix::identity(order.size());
  M(order.position(fromstate)+1,order.position(fromstate)+1)=0;
  M(order.position(tostate)+1,order.position(fromstate)+1)=1;
  return M;
}



Vector::Vector(const string &name, const Order &order):Matrix(order.size(),1)
{
  operator()(order.position(name)+1,1)+=1;
}

Vector Vector::zero(const Order &order)
{
  Matrix M=Matrix(order.size(),1);
  M.fill(0);
  return M;
}
