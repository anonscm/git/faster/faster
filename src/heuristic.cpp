/*     
   FAST, an accelerated symbolic model-checker. 
   Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, Alain Finkel (coordinator) and LSV,
   CNRS UMR 8643 & ENS Cachan.

   This file is part of FAST.

   FAST is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   FAST  is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with FAST; see the file COPYING.  If not, write to
   the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*/


// ATTENTION : tout est en place pour choix des reductions et heuristiques, mais pas encore pris en//  compte.


#include "heuristic.h"

int MAX_STATES=0;
int MAX_ACCELERATIONS=0; 
int HEURISTIC=0; // 0 best first ; 1 depth first
int ACCELERATION=0; // 0  standard ; 1 convex ; 2 positive ; 3 post  
int VERBOSE=1;

bool red_conj=true;
bool red_commutation=true;
bool red_union=true;




LNDD DownwardClosure(const LNDD &states)
{
  LNDD res=states;
  Order global=Relation::makeRelationOrder(states.order);


  Order order = states.order;  

  // ************ trop gourmand en calculs, leq est enorme
  /*  Relation Leq = Relation::leq(states.order,prime);
 
cerr<<"("<<Leq.numberOfStates()<<" bdd nodes)"<<endl;  

res = compose(Leq,states);*/


    for(int i=0;i<(int)states.order.size();i++){
      LNDD leqi=  LNDD(LinearSum(order[i])-LinearSum(PrimeVariables::prime(order[i])),">=0",global)
      //  & LNDD(LinearSum(order[i]),">=0", global)
            &  LNDD(LinearSum(PrimeVariables::prime(order[i])),">=0",global)
      //& tout
            ;             
              // *********** le & doit pas etre bon, fait inferieur par composante  ***********
    for(int j=0;j<(int)order.size();j++) if (i!=j) {
      leqi=leqi&LNDD(-LinearSum(order[j])+LinearSum(PrimeVariables::prime(order[j])),"==0",global);
    }
    res=compose(Relation(leqi,order),res);
    }  //   ***************************
  

    return res;
    
}



Relation  computeSuccessorRelation (LinearSystem::const_iterator  i) // moralement un Affine*
{

  if  (ACCELERATION==0) return  i->startoRelation(); 
  if  (ACCELERATION==1) return  i->convexStartoRelation();   
  if  (ACCELERATION==2) return  i->positiveStartoRelation(); 
  if  (ACCELERATION==3) return  (i->toRelation() | Relation::equality(i->order)); 

  exit(0);  
}



LNDD compute(const LinearSystem &L, const LNDD &states, bool direction) // integrer le downward closure
{
  VectorRelation vec;
  VectorString vecName;
  // On calcule l'ensemble des relations possibles.
  int count_max=(int)L.size();
  int count_i=0;
  int total_size=0;
  for(LinearSystem::const_iterator i=L.begin();i!=L.end();i++) {

    
    Relation relation = computeSuccessorRelation(i);
    
  
    // *******************************************************
    //Relation relation=i->startoRelation();    
    //Relation relation=i->convexStartoRelation();      // pour tests  
    //Relation relation=i->positiveStartoRelation();      // pour tests    
    //Relation relation=i->toRelation() | Relation::equality(states.order,L.prime) ;// pour tests
    // ******************************************************



    vec.push_back(relation);
    vecName.push_back(i->name);
    count_i++;
    if (VERBOSE)
	cerr<<"("<<count_i<<"/"<<count_max<<")"
	    <<", "
	    <<"name = "<<i->name
	    <<", "
	    <<"size = "<<relation.size()
	    <<endl;
    total_size+=(int)relation.size();

   
  }

 
  


  // je pense que le calcul des fonctions possibles  devrait etre une fonction a part, rien a foutre dans heuristique  
  
  LNDD S=states;
  LNDD Snew=S;
  LNDD Snext=S;
  bool found;
  int ncycles=0;
  bool firstrandom = true;
  int  nextrandom = -1;
  

  PickUP pUPtot(vec.size());
  do {    
    PickUP pUP=pUPtot;
    if (VERBOSE) {
	cerr<<"**********************"<<endl;
	cerr<<"Random choose"<<endl;
    }
    found=false;
    int choix = -1;    
    // On cherche une acceleration qui nous donne un ensemble Snew different de S. sur le premier random on teste tout.
    if (firstrandom)
      {while ((!(found)) && (((int)pUP.size())>0)) {
	choix=pUP.randomGet();
	Relation relation=vec[choix];
	string name=vecName[choix];
	if (VERBOSE)
	    cerr<<"Name = "<<name<<" ";
	
        if (direction){	  
	  Snew=compose(relation,S);// Forward algorithm
	} else
	  Snew=compose(S,relation);// Backward algorithm
	
        if (S==Snew) {
	  if (VERBOSE)
	      cerr<<" No new state computed"<<endl;
	} else {
	  if (VERBOSE)
	      cerr<<" OK !"<<endl;
	  ncycles++;
	  found=true;
	}
      }     
      firstrandom = false;
      } else {                                            // Apres le premier random on se sert du resultat du minimize
	choix=nextrandom;
	Snew=Snext;	
	string name=vecName[choix];
	if (VERBOSE)
	    cerr<<"Name = "<<name<<" ";
	if (S==Snew)                                          // enlever ce test qd on sera sur que ca marche
	  cerr<<" No new state computed"<<"EEEEEEEERRRRRRRRRRRRRROOOOOOOOOOOOOOORRRRRRRRRRRRR" <<endl;
	else {
	  if (VERBOSE)
	      cerr<<" OK !"<<endl;
	  ncycles++;
	  found=true;
	}		
      }
    
    
    
    if (found) {
      if (VERBOSE) {
	  cerr<<"Now we have a representation of size = "<<Snew.size()<<endl;
	  cerr<<"Minimization in Progress"<<endl;
      }
      pUP=pUPtot;
      pUP.remove(choix);
      bool increase = false;
      while (pUP.size()>0) {
        choix=pUP.randomGet();
        Relation relation=vec[choix];
        string name=vecName[choix];
	if (VERBOSE)
	    cerr<<"Name = "<<name<<" ";
        LNDD Stmp=Snew;

        if (direction){	  
          Stmp=compose(relation,Snew);// Forward algorithme
        }
        else
          Stmp=compose(Snew,relation);// Backward algorithme
        
        if (Stmp==Snew) { // bizarre ce if else non ? on devrait juste minimiser nb states et la on teste taille des ensembles representes
	  if (VERBOSE)
	      cerr<<"No new state computed\n";
        } else
          if (Stmp.size()<=Snew.size()) {
            Snew=Stmp;
            ncycles++;
            pUP=pUPtot;
	    pUP.remove(choix);
	    increase=false;
	    if (VERBOSE)
		cerr<<" OK !"<<endl
		    <<"Now we have a representation of size = "<<Snew.size()<<endl;
          }
          else
            {
	    if (VERBOSE)
		cerr<<" I.N.C.R.E.A.S.E"<<endl; 
	    increase=true;
	    nextrandom=choix;
	    Snext=Stmp;}	
      }      
      if (VERBOSE)
	  cerr<<"increase = "<<increase<<endl;
      found=increase; // astuce pour pas tester un coup de trop qd la minimisatin permet de conclure un  point fixe      
      S=Snew;      
    }    
  } while (found && 
	   ((MAX_STATES==0) ||(Snew.size()<MAX_STATES))&&
	   ((MAX_ACCELERATIONS==0) ||(ncycles<MAX_ACCELERATIONS))); 
  if (VERBOSE)
      cerr<<"nb accelerations "<<ncycles<<endl;
  return S;
}



LNDD computedepthfirst(const LinearSystem &L, const LNDD &states, bool direction)
{
  VectorRelation vec;
  VectorString vecName;
  // On calcule l'ensemble des relations possibles.
  int count_max=L.size();
  int count_i=0;
  float total_size=0;
  for(LinearSystem::const_iterator i=L.begin();i!=L.end();i++) {
     
    Relation relation = computeSuccessorRelation(i);

    // *******************************************************
    //Relation relation=i->startoRelation();      // pour tests
    //Relation relation=i->convexStartoRelation();      // pour tests  
    //Relation relation=i->positiveStartoRelation();      // pour tests    
    // ******************************************************

    vec.push_back(relation);
    vecName.push_back(i->name);
    count_i++;
    cerr<<"("<<count_i<<"/"<<count_max<<")"
        <<", "
        <<"name = "<<i->name
        <<", "
        <<"size = "<<relation.size()
        <<endl;
    total_size+=relation.size();
  
 
  }

 

  // je pense que le calcul des fonctions possibles  devrait etre une fonction a part, rien a foutre dans heuristique  

  LNDD S=states;
  LNDD Snew=S;
  LNDD Snext=S;
  bool found;
  int ncycles=0;
  //bool firstrandom = true;
  //int  nextrandom = -1;
  int choix;
    
  PickUP pUPprev(vec.size());

  do {
    S=Snew;    
    cerr<<"***************************\n* We have used "<<ncycles<<" acceleration for the moment"
        <<endl
        <<"* representation of size =  "<<S.size()
        <<endl
        <<"***************************"
        <<endl;
    found=false;
    PickUP pUP=pUPprev;
    while (pUP.size()>0) {
       choix=pUP.randomGet();
      Relation relation=vec[choix];
        string name=vecName[choix];
        cerr<<"Name = "<<name<<" ";
        if (direction){	  
          Snew=compose(relation,S);// Forward algorithme
        }
        else
          Snew=compose(S,relation);// Backward algorithme

        if (S==Snew) {
          cerr<<"No new state computed\n";
        }	
        else {	  
	found=true;
	S=Snew;
	ncycles++;	
	cerr<<"representation of size = "<<S.size()<<endl;
	}
	
    }
    

  } while ((found) && 
           ((MAX_STATES==0) || (Snew.size()<MAX_STATES)) &&   
           ((MAX_ACCELERATIONS==0) || (ncycles<MAX_ACCELERATIONS)) 
           );

  cout<<"nb accelerations : "<<ncycles<<endl;
  return S;  
}





LNDD heuristique(const LinearSystem &Ls,
                 const LNDD &startStates,
                 bool direction,
                 int k=1)
{
  LinearSystem L=Ls;   
  
  LNDD states=startStates;
  

  //Computation of the cycle of length <=k
  LinearSystem Lnow=L;
  for(int i=1;i<k;i++)
    Lnow=compose(Lnow,L).simplify(); 
  //Lnow=compose(Lnow,L); // no reduction par union
    

  int lengthMax=k;
  while (true) {
    if (VERBOSE)
	cerr<<"cycles of length "<<lengthMax<<endl;
    if (HEURISTIC==0) states=compute(Lnow,startStates,direction);  
    if (HEURISTIC==1) states=computedepthfirst(Lnow,startStates,direction);// depthfirst heuristics
    //states=computeClosureDepthfirst(Lnow,startStates,direction);  // depth first and dc

    if (L.fixPoint(states,direction)) {
      if (VERBOSE)
	  cerr<<"Fix point FOUND\n";
      return states;
    }
    if (VERBOSE)
	cerr<<"All the cycles considered have been used without succes.\n"; 
    //On prend des cycles plus grand
    lengthMax++;
    if (VERBOSE)
	cerr<<"Inflating the linear system"<<endl;
    Lnow=compose(Lnow,L).simplify();
    }
  			     
     return states;
     
 
}

