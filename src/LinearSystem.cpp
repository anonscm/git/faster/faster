

#include "LinearSystem.h"
#include "Relation.h"
#include "Affine.h"

bool LinearSystem::fixPoint(const LNDD & set, const bool direction) const
{
  if (direction)
    for(int i=0;i<(int)size();i++){
      LNDD f_i_of_set=compose(operator[](i).toRelation(),set);
      if (!(f_i_of_set<=set)) return false;
    }
  else 
    for(int i=0;i<(int)size();i++){
      LNDD f_i_of_set=compose(set,operator[](i).toRelation());
      if (!(f_i_of_set<=set)) return false;
    }  
  return true;
}


void LinearSystem::insert(const Affine &f)
{
  push_back(f);
}

LinearSystem::LinearSystem(const Order &order, const string &name):vector<Affine>(),order(order),name(name)
{
}

LinearSystem::LinearSystem():  vector<Affine>(), order(),name("")
{
}

LinearSystem LinearSystem::removeGuard(void) const
{
  LinearSystem L(order,name+" Without Guard");
  for(int i=0;i<(int)size();i++)
    L.insert(operator[](i).removeGuard());
  return L;
}

LinearSystem compose(const LinearSystem & L1,const LinearSystem & L2)
{
  //IL faudrait verifier legalite des ordre order et prime
  Order order=L1.order;
  
  int count_max=((int)L1.size())*((int)L2.size());  // la taille est pas toujours celle de T^k 
  int count_i=0;  
  LinearSystem L(order);

  for(LinearSystem::const_iterator i=L1.begin();i!=L1.end();i++)
    for(LinearSystem::const_iterator j=L2.begin();j!=L2.end();j++) {
      Affine fij=compose(*i,*j); 
      Affine fji=compose(*j,*i); 


      if (!(fij==fji)) // ************ simplif par commutation *************
          {if ( !(fij.guard.isEmpty())   ) L.insert(fij);} 

      //if ( !(fij.guard.isEmpty())   ) L.insert(fij);


// ******************* conjugaison + commut au rang 2
//  for(int i=0;i<L1.size();i++)
//    for(int j=0;j<i;j++) {
//      Affine fij=compose(L1[i],L2[j]); 
//      Affine fji=compose(L2[j],L1[i]);
// 
//       if (!(fij==fji)) // ************ simplif par commutation *************
//           {if ( !(fij.guard.isEmpty())   ) L.insert(fij);}       
//      
//       //if ( !(fij.guard.isEmpty())   ) L.insert(fij);
// ******************* fin



      count_i++;
      if (VERBOSE)
	  cerr<<"("<<count_i<<"/"<<count_max<<")->"<<endl;     
    }  

  for(LinearSystem::const_iterator i=L1.begin();i!=L1.end();i++)  
    L.insert(*i);  // *** remettre vieilles fonctions de L1

    //   for(LinearSystem::const_iterator i=L2.begin();i!=L2.end();i++) 
    //L.insert(*i);   // *** inutile, on remet en trop
  
  //L.insert(Affine::identity(L.order,L.prime));  // marche pas dans le cas 1

  return L;//LinearSystem::simplify(L);
}




class MatrixVector
{
public:
  SquareMatrix M;
  Vector v;
  MatrixVector(const SquareMatrix &M, const Vector &v):M(M),v(v)
  {
  }
};


bool operator<(const MatrixVector & Mv1, const MatrixVector & Mv2)
{
  return ((Mv1.M<Mv2.M)||((Mv1.M==Mv2.M)&&(Mv1.v<Mv2.v)));
}


#include <vector>
typedef vector<const Affine*> vectorpAffine;


#include <map>
typedef map<MatrixVector,vectorpAffine> FlatLinearSystem;

LinearSystem LinearSystem::simplify(void) const  
{
  //Il faut chercher l'ensemble des actions possibles
  if (VERBOSE)
      cerr<<"Simplification of the linear system "<<name<<endl;
  FlatLinearSystem Lflat;
  for(const_iterator p=begin();p!=end();p++){
    Lflat[MatrixVector(p->M,p->v)].push_back(&(*p));
  }  
  
  LinearSystem Lsimplify(order,"simplify("+name+")");
  for(FlatLinearSystem::const_iterator p=Lflat.begin();p!=Lflat.end();p++){
    const vectorpAffine &vec=p->second;
    if (VERBOSE)
	cerr<<vec.size()<<" functions have the same action";
   
    
    vectorpAffine::const_iterator pos=vec.begin();
    string fname=(*pos)->name;
    LNDD fguard=(*pos)->guard;
    pos++;
    while (pos!=vec.end()) {
      fguard=fguard|(*pos)->guard;
      fname=fname+"+"+(*pos)->name;
      pos++;
    }
    if (VERBOSE)
	cerr<<", it remains only 1"<<endl; // on ne teste plus si garde est vide, fait avant
     if ((int)vec.size()>1)
       fname="("+fname+")";
     Lsimplify.insert(Affine(p->first.M,p->first.v,fguard,order,fname));
  } 
  if (VERBOSE)
      cerr
	<<"Number of actions before simplification : "<<size()<<endl
	<<"Number of actions after simplification  : "<<Lsimplify.size()<<endl;
  return Lsimplify;
}


