#! /bin/sh

SEEDS=`cat seeds.txt`
BENCHMARKS="benchmarks.lst"
IGNORED_SOLVERS="\\(genz\\)\\|\\(ppl\\)"
SOLVERS=`distiller -q --solvers | sed "s/${IGNORED_SOLVERS}//g"` 

SOLVERS=`(for s in ${SOLVERS}; do echo $s; done) | sort`
BENCH_URL='http://altarica.labri.fr/CVSweb/cvsweb.cgi/faster/benchmark/@DIR@/@BENCH@.fst?rev=HEAD'
BENCHFILES=`cat ${BENCHMARKS}` 

function bench_url {
    dir="$1"
    bench="$2"

    echo ${BENCH_URL} | sed -e "s/@BENCH@/${bench}/g" -e "s/@DIR@/${dir}/g"
}

function bench_link {
    dir="$1"
    bench="$2"
    label="$2"
    if test `echo $label | wc -c` -gt 10; then
	label="`printf "%.10s" ${label}`..."
    fi
    echo "[[`bench_url ${dir} ${bench}`|${label}]]"
}

function std_results {
    echo -n "^  File  ^  Seed"
    for S in ${SOLVERS}; do
	echo -n "  ^  ${S}"
    done
    
    echo "  ^"

    for B in ${BENCHFILES}; do
	prefix=`basename ${B} .fst`
       
	for G in ${SEEDS}; do	
	    echo -n "^`bench_link fst-no-batof ${prefix}`  ^  ${G}"
	    for S in ${SOLVERS}; do
		result="fst-no-batof/${prefix}-${G}-${S}.result"
		errors="${result}.err"
		time="${result}.time"
		echo -n "  |  "
		
		if [ ! -f ${result} ] || [ "x`cat ${result}`" == "xerror" ] || \
                   [ ! -s ${time} ] ; 
		then
		    echo -n "ERR"
		else
		    echo -n `grep "user" ${time} | sed 's/user //g'`
		fi
	    done
	    echo "  |"
	done
    done
}

function batof_results {
    G=`echo ${SEEDS} | awk '{ print $1 }'`
    echo "Seed = $G"
    echo ""
    echo "^  File  ^  <html>formula size<br> in nodes</html>  ^  <html>formula size<br> in chars</html>  ^  <html>automata<br> size</html>  ^  <html>total<br> time</html>  ^  <html>synthesis<br> time</html>  ^"

    for B in ${BENCHFILES}; do
	prefix=`basename ${B} .fst`
	same_results="yes"


	    result="fst/${prefix}-${G}.result"
	    errors="${result}.err"
	    time="${result}.time"
	    
  	    # input
	    echo -n "^`bench_link fst ${prefix}`  |  "
	
	    if [ ! -f ${result} ] || [ "x`cat ${result}`" == "xerror" ] || \
                [ ! -s ${time} ] ; 
	    then
		echo "ERR  |||||"
		continue
	    fi

	    # formula size in nodes
	    FSIN=`grep -e "// Formula size *:" ${result} | sed -e 's,//.*: \([0-9][0-9]*\) nodes,\1,g'`

	    # formula size in chars
	    FSIC=`wc -c ${result} | awk '{ print $1 }'`

     	    # automata size
	    AS=`grep -e "// Automata size *:" ${result} | sed -e 's,//.*#S=\([0-9][0-9]*\) .*,\1,g'`
	    # total time
	    TT=`grep "user" ${time} | sed 's/user //g'`
	    # synthesis time
	    ST=`grep -e "// Time to synthesis formula *:" ${result} | sed -e 's,//.*: \([0-9][0-9]*\) ms,\1,g' | awk '{ print $1/1000 }'`
	    echo "${FSIN}  |  ${FSIC}  |  ${AS}  |  ${TT}  |  ${ST}  |"

    done
}

std_results > std-results.dokuwiki
batof_results > batof-results.dokuwiki


exit 0
