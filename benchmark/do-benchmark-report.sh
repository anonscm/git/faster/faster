#! /bin/sh

SEEDS=`cat seeds.txt`
BENCHMARKS="benchmarks.lst"
SOLVERS=`distiller -q --solvers`
BENCHFILES=`cat ${BENCHMARKS}` 

function std_results {
    echo -n "input; seed"
    for S in ${SOLVERS}; do
	echo -n "; ${S}"
    done
    
    echo

    for B in ${BENCHFILES}; do
	prefix=`basename ${B} .fst`
       
	for G in ${SEEDS}; do	
	    echo -n "${prefix};${G}"
	    for S in ${SOLVERS}; do
		result="fst-no-batof/${prefix}-${G}-${S}.result"
		errors="${result}.err"
		time="${result}.time"
		echo -n ";"
		
		if [ ! -f ${result} ] || [ "x`cat ${result}`" == "xerror" ] || \
                   [ ! -s ${time} ] ; 
		then
		    echo -n "ERR"
		else
		    echo -n `grep "user" ${time} | sed 's/user //g'`
		fi
	    done
	    echo
	done
    done
}

function batof_results {
    echo "input; seed; formula size in nodes; formula size in chars; automata size; total time; synthesis time"

    for B in ${BENCHFILES}; do
	prefix=`basename ${B} .fst`

	for G in ${SEEDS}; do	
	    result="fst/${prefix}-${G}.result"
	    errors="${result}.err"
	    time="${result}.time"
	    
  	    # input
	    echo -n "${prefix}; ${G};"
	
	    if [ ! -f ${result} ] || [ "x`cat ${result}`" == "xerror" ] || \
                [ ! -s ${time} ] ; 
	    then
		echo "ERR"
		continue
	    fi

	    # formula size in nodes
	    FSIN=`grep -e "// Formula size *:" ${result} | sed -e 's,//.*: \([0-9][0-9]*\) nodes,\1,g'`

	    # formula size in chars
	    FSIC=`wc -c ${result} | awk '{ print $1 }'`

     	    # automata size
	    AS=`grep -e "// Automata size *:" ${result} | sed -e 's,//.*#S=\([0-9][0-9]*\) .*,\1,g'`
	    # total time
	    TT=`grep "user" ${time} | sed 's/user //g'`
	    # synthesis time
	    ST=`grep -e "// Time to synthesis formula *:" ${result} | sed -e 's,//.*: \([0-9][0-9]*\) ms,\1,g' | awk '{ print $1/1000 }'`
	    echo "${FSIN};${FSIC};${AS};${TT};${ST}"
	done 
    done
}

std_results > std-results.csv
batof_results > batof-results.csv


exit 0
