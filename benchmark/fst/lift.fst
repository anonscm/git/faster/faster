/**
 * FAST, an accelerated symbolic model-checker. 
 * Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, 
 * Alain Finkel (coordinator) and LSV, CNRS UMR 8643 & ENS Cachan.
 *
 * FAST is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation; either version 2, or (at your option) any later version.
 *
 * FAST  is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FAST; see the file COPYING.  If not, write to the Free Software Foundation, 
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */
model lift {


var a,c,g,N;

states e0;

transition r1 := {
	from :=e0;
	to:=e0;
	guard :=
		c=g && g>1
	;
	action :=
		g'=g-1
	;
};


transition r2 := {
	from :=e0;
	to:=e0;
	guard :=
	c=g && g<N
	;
action :=
	g'=g+1
	;
};




transition r3 :=  {
	from :=e0;
	to:=e0;
	guard :=
	c>g && a=1
	;
	action :=
	a'=0
	;
};

transition r4  := {
	from :=e0;
	to:=e0;
	guard :=
		c<g && a=1
	;
	action :=
		a'= 2
	;
};

transition r5  :={
	from :=e0;
	to:=e0;
	guard :=
		!a=1
	;
action :=
	c'=c+a-1,
	a'=1
	;
};

}


strategy s1 {

setMaxState(0);
setMaxAcc(100);


Region init :=
	{state=e0 && c=0 && g=0 && a=1 && N>=0};

Transitions t := {r1,r2,r3,r4,r5};

Region reach := post*(init, t,3);

print(reach);	
}
