/**
 * FAST, an accelerated symbolic model-checker. 
 * Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, 
 * Alain Finkel (coordinator) and LSV, CNRS UMR 8643 & ENS Cachan.
 *
 * FAST is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation; either version 2, or (at your option) any later version.
 *
 * FAST  is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FAST; see the file COPYING.  If not, write to the Free Software Foundation, 
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */
model incdec {




var  whileinc, incx, xi, notifyincx, endincx, incy, yi, notifyincy, endincy, endinc, whiledec, decx, while1, wait1, afterwait1, xd, enddecx1, enddecx2, decy, while2, wait2, afterwait2, yd, enddecy1, enddecy2, enddec, xpos, ypos, lock;

/* on utilise 
 xpos + notxpos = 1, ypos + notypos = 1, lock+ unlock=1
*/


states normal;


transition t1 := { 
	from:=normal;
	to:=normal;
	guard:=	
	  whileinc >= 1;
action:=
	whileinc' = whileinc - 1
	,
	incx' = incx + 1;
};


transition t2 :={ from:=normal;to:=normal;
guard :=	
	  incx >= 1 && lock = 0 
;action:=
	incx' = incx - 1
	,
	xi' = xi + 1
	,
	lock' =  1


;}; transition t3 :={ from:=normal;to:=normal;
guard :=	
	  xi >= 1 
	;action:=
	xi' = xi - 1
	,
	notifyincx' = notifyincx + 1
	,
	xpos' = 1 


;}; transition t4 :={ from:=normal;to:=normal;
guard :=	
	  notifyincx >= 1 
;action:=
	notifyincx' = notifyincx - 1
	,
	endincx' = endincx + 1
	,
	afterwait1' = afterwait1 + wait1 
	,
	wait1' = 0
	,
	afterwait2' = afterwait2 + wait2 
	,
	wait2' = 0


;}; transition t5 :={ from:=normal;to:=normal;
guard :=	
	  endincx >= 1 && lock = 1 
;action:=
	endincx' = endincx - 1
	,
	incy' = incy + 1
	,
	lock' = 0
	


;}; transition t6 :={ from:=normal;to:=normal;
guard :=	
	  incy >= 1 && lock = 0 
;action:=
	incy' = incy - 1
	,
	yi' = yi + 1
	,
	lock' =  1


;}; transition t7 :={ from:=normal;to:=normal;
guard :=	
  	  yi >= 1 
;action:=
	yi' = yi - 1
	,
	notifyincy' = notifyincy + 1
	,
	ypos' = 1


;}; transition t8 :={ from:=normal;to:=normal;
guard :=	
	  notifyincy >= 1 
;action:=
	notifyincy' = notifyincy - 1
	,
	endincy' = endincy + 1
	,
	afterwait1' = afterwait1 + wait1 + 0
        ,
        wait1' = 0
        ,
        afterwait2' = afterwait2 + wait2 + 0
        ,
        wait2' = 0

	
;}; transition t9 :={ from:=normal;to:=normal;
guard :=
	 endincy >= 1 && lock = 1 
;action:=
	endincy' = endincy - 1
	,
	endinc' = endinc + 1
	, 
	lock' = 0

	
;}; transition t10 :={ from:=normal;to:=normal;
guard :=
	  endinc >= 1 
;action:=
	endinc' = endinc - 1
	,
	whileinc' = whileinc + 1
	
;}; transition t11 :={ from:=normal;to:=normal;
guard :=
	  whiledec >= 1 
;action:=
	whiledec' = whiledec - 1
	,
	decx' = decx + 1

	
;}; transition t12 :={ from:=normal;to:=normal;
guard :=
	  decx >= 1 && lock = 0 
;action:=
	decx' = decx - 1
	,
	while1' = while1 + 1
	,
	lock' = 1

	
;}; transition t13 :={ from:=normal;to:=normal;
guard :=
	  while1 >= 1 && lock = 1 && xpos = 0 
;action:=
	while1' = while1 - 1
	,
	wait1' = wait1 + 1
	,
	lock' = 0

	
;}; transition t14 :={ from:=normal;to:=normal;
guard :=
	  afterwait1 >= 1 && lock = 0 
;action:=
	afterwait1' = afterwait1 - 1
	,
	while1' = while1 + 1
	,
	lock' = 1

	
;}; transition t15 :={ from:=normal;to:=normal;
guard :=
	  while1 >= 1 && xpos = 1 
;action:=
	while1' = while1 - 1
	,
	xd' = xd + 1

	
;}; transition t16 :={ from:=normal;to:=normal;
guard :=
	  xd >=1 
;action:=
	xd' = xd - 1
	,
	enddecx1' = enddecx1 + 1
	,
	xpos' = 1

	
;}; transition t17 :={ from:=normal;to:=normal;
guard :=
	  xd >=1 
;action:=
        xd' = xd - 1
        ,
        enddecx2' = enddecx2 + 1
        ,
        xpos' = 0

	
;}; transition t18 :={ from:=normal;to:=normal;
guard :=
	  enddecx1 >= 1 && lock = 1 
;action:=
	enddecx1' = enddecx1 - 1
	,	
	decy' = decy + 1
	,
	lock' = 0

	
;}; transition t19 :={ from:=normal;to:=normal;
guard :=
	  enddecx2 >= 1 && lock = 1 
;action:=
        enddecx2' = enddecx2 - 1
        ,
        decy' = decy + 1
        ,
        lock' = 0

	
;}; transition t20 :={ from:=normal;to:=normal;
guard :=
	  decy >= 1 && lock = 0 
;action:=
	decy' = decy - 1
	,
	while2' = while2 + 1
	,
	lock' =  1

	
;}; transition t21 :={ from:=normal;to:=normal;
guard :=
	  while2 >= 1 && ypos = 0 && lock = 1 
;action:=
	while2' = while2 - 1
	,
	wait2' = wait2 + 1
	,
	lock' = 0
	
	
;}; transition t22 :={ from:=normal;to:=normal;
guard :=
	  afterwait2 >= 1 && lock = 0 
;action:=
	afterwait2' = afterwait2 - 1
	,
	while2' = while2 + 1
	,
	lock' = 1

	
;}; transition t23 :={ from:=normal;to:=normal;
guard :=
	  while2 >= 1 && ypos = 1 
;action:=
	while2' = while2 - 1
	,
	yd' = yd + 1

	
;}; transition t24 :={ from:=normal;to:=normal;
guard :=
	  yd >= 1 
;action:=
	yd' = yd - 1
	,
	enddecy1' = enddecy1 + 1
	,
	ypos' = 1 

	
;}; transition t25 :={ from:=normal;to:=normal;
guard :=
	  yd >= 1 
;action:=
        yd' = yd - 1
        ,
        enddecy2' = enddecy2 + 1
        ,
        ypos' = 0 

	
;}; transition t26 :={ from:=normal;to:=normal;
guard :=
	  enddecy1 >= 1 && lock = 1 
;action:=
	enddecy1' =enddecy1 - 1
	,
	enddec' = enddec + 1
	,
	lock' = 0

	
;}; transition t27 :={ from:=normal;to:=normal;
guard :=
	  enddecy2 >= 1 && lock = 1 
;action:=
        enddecy2' =enddecy2 - 1
        ,
        enddec' = enddec + 1
        ,
        lock' = 0

	
;}; transition t28 :={ from:=normal;to:=normal;
guard :=
	  enddec >= 1 
;action:=
	enddec' = enddec - 1
	,
	whiledec' = whiledec + 1
;}; 

}

strategy s1 {

setMaxState(0);
setMaxAcc(3000);


Region init :=
	{state=normal &&
 whileinc >= 1 && 
incx  = 0 && 
xi = 0 && 
notifyincx = 0 && 
endincx = 0 && 
incy = 0 && 
yi = 0 && 
notifyincy = 0 && 
endincy = 0 && 
endinc = 0 && 
whiledec >= 1 && 
decx = 0 && 
while1 = 0 && 
wait1 = 0 && 
afterwait1 = 0 && 
xd = 0 && 
enddecx1 = 0 && 
enddecx2 = 0 && 
decy = 0 && 
while2 = 0 && 
wait2 = 0 && 
afterwait2 = 0 && 
yd = 0 && 
enddecy1 = 0 && 
enddecy2 = 0 && 
enddec = 0 && 
xpos = 0 &&  
ypos = 0 &&  
lock = 0 
};

//Region unsafe :={ decy + incy >= 2};

Transitions t := {t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23,t24,t25,t26,t27,t28};

Region reach := post*(init, t,4);

//Region reach := pre*(unsafe,t,1);
print (reach);	
}
