/**
 * FAST, an accelerated symbolic model-checker. 
 * Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, 
 * Alain Finkel (coordinator) and LSV, CNRS UMR 8643 & ENS Cachan.
 *
 * FAST is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation; either version 2, or (at your option) any later version.
 *
 * FAST  is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FAST; see the file COPYING.  If not, write to the Free Software Foundation, 
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */
model BERKELEY {

var   invalid, unowned,nonexclusive,exclusive;

states normal;


transition r1  :=  {
	from :=normal;
	to:=normal;
	guard :=
		invalid>=1
	;
	action :=  
		nonexclusive'=nonexclusive+exclusive,
	      	exclusive'=0,
		invalid'=invalid-1,
		unowned'=unowned+1
	;
};  	      

transition r2  :=  {
	from :=normal;
	to:=normal;
	guard :=
           	nonexclusive + unowned >=1
	;
	action := 
		invalid'=invalid + unowned + nonexclusive-1,
		exclusive'=exclusive+1,
		unowned'=0,
		nonexclusive'=0     	
	;
};

transition r3  :=  {
	from :=normal;
	to:=normal;
	guard :=
		invalid>=1 
	;
	action := 
		unowned'=0,
		nonexclusive'=0,
		exclusive'=1,
		invalid'=invalid+unowned+exclusive+nonexclusive-1
	;
};

}


strategy s1 {

setMaxState(0);
setMaxAcc(100);


Region init :=
	{state=normal && exclusive=0 && nonexclusive=0 && unowned=0 && invalid>0};

Transitions t := {r1,r2,r3};

Region reach := post*(init, t);

}
