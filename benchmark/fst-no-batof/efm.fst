/**
 * FAST, an accelerated symbolic model-checker. 
 * Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, 
 * Alain Finkel (coordinator) and LSV, CNRS UMR 8643 & ENS Cachan.
 *
 * FAST is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation; either version 2, or (at your option) any later version.
 *
 * FAST  is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FAST; see the file COPYING.  If not, write to the Free Software Foundation, 
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

model efm {

var X1,X2,X3,X4,X5,X6;

states normal;

transition t1 := {
from := normal;
to := normal;
guard := X1>=1 && X4>=1;
action := X1'=X1-1, X4'=X4-1, X2'=X2+1, X5'=X5+1;
};

transition t2 := {
from := normal;
to := normal;
guard := X2>=1 && X6>=1;
action := X2'=X2-1, X3'=X3+1;
};

transition t3 := {
from := normal;
to := normal;
guard := X4>=1 && X3>=1;
action := X3'=X3-1, X2'=X2+1;
};

transition t4 := {
from := normal;
to := normal;
guard := X3>=1;
action := X3'=X3-1, X1'=X1+1, X6'=X6+X5, X5'=0;
};

transition t5 := {
from := normal;
to := normal;
guard := X2>=1;
action := X2'=X2-1, X1'=X1+1, X4'=X4+X6, X6'=0;
};

}



strategy s1 {

setMaxState(0);
setMaxAcc(100);

Region init := {X1>=1 && state=normal && X2=0 && X3=0 && X4=1 && X5=0 && X6=0};
Transitions t := {t1,t2,t3,t4,t5};

Region reach := post*(init, t);
}
