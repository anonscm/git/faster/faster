/**
 * FAST, an accelerated symbolic model-checker. 
 * Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, 
 * Alain Finkel (coordinator) and LSV, CNRS UMR 8643 & ENS Cachan.
 *
 * FAST is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation; either version 2, or (at your option) any later version.
 *
 * FAST  is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FAST; see the file COPYING.  If not, write to the Free Software Foundation, 
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

model BoigelotLegayWolperCAV03 {
  var  x,xp,y,yp,k; 

  states normal;

  transition f1 := {
    from := normal;
    to := normal;
    guard := true;
    action :=
      xp'=xp+1,
      yp'=yp+1,
      k'=k+1;
  };

  transition f2 := {
    from :=normal;
    to := normal;
    guard := yp>=1;
    action :=
      xp'=xp+2,
      yp'=yp-1,
      k'=k+1;
  };

  transition f3   := {
    from := normal;
    to := normal;
    guard := xp>=1;
      action:= 
      xp'=xp-1,
      yp'=yp+2,
      k'=k+1;	
  };
}

strategy s1 {
  setMaxState(0);
  setMaxAcc(100);
  Region I := {state=normal && x=xp && y=yp && k=0};

  Transitions set1 := {f1};
  Region I1 := post*(I, set1,1);

  Transitions set23 := {f2,f3};
  Region I2 := post*(I1, set23,2);

  Transitions set123 := {f1,f2,f3};
  Region R := post*(I2, set123,1);
}
