/**
 * FAST, an accelerated symbolic model-checker. 
 * Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, 
 * Alain Finkel (coordinator) and LSV, CNRS UMR 8643 & ENS Cachan.
 *
 * FAST is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation; either version 2, or (at your option) any later version.
 *
 * FAST  is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FAST; see the file COPYING.  If not, write to the Free Software Foundation, 
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */
model canibals_and_missionaries_v2 {

var c_left, c_right, c_boat, m_left, m_right, m_boat, max_in_boat;

states boat_on_left_side, boat_on_right_side, finished;

transition EndOfTravel := {
 from   := boat_on_right_side;
 to     := finished;
 guard  := c_left=0 && c_boat=0 && m_left=0 && m_boat=0;
 action := c_left'=c_left;
};

transition LoadCanibalsOnLeftSide := {
 from   := boat_on_left_side;
 to     := boat_on_left_side;
 guard  := c_left>0 && c_boat+1<m_boat && c_boat+m_boat<max_in_boat;
 action := c_boat'=c_boat+1, c_left'=c_left-1;
};

transition LoadMissionariesOnLeftSide := {
 from   := boat_on_left_side;
 to     := boat_on_left_side;
 guard  := m_left>0 && c_boat<m_boat+1 && c_boat+m_boat<max_in_boat;
 action := m_boat'=m_boat+1, m_left'=m_left-1;
};

transition CrossAndUnloadToRightSide := {
 from   := boat_on_left_side;
 to     := boat_on_right_side;
 guard  := c_right+c_boat<=m_right+m_boat && c_left<=m_left;
 action := c_right'=c_right+c_boat, m_right'=m_right+m_boat, c_boat'=0, m_boat'=0;
};

transition LoadCanibalsOnRightSide := {
 from   := boat_on_right_side;
 to     := boat_on_right_side;
 guard  := c_right>0 && c_boat+1<m_boat && c_boat+m_boat<max_in_boat;
 action := c_boat'=c_boat+1, c_right'=c_right-1;
};

transition LoadMissionariesOnRightSide := {
 from   := boat_on_right_side;
 to     := boat_on_right_side;
 guard  := m_right>0 && c_boat<m_boat+1 && c_boat+m_boat<max_in_boat;
 action := m_boat'=m_boat+1, m_right'=m_right-1;
};

transition CrossAndUnloadToLeftSide := {
 from   := boat_on_right_side;
 to     := boat_on_left_side;
 guard  := c_left+c_boat<=m_left+m_boat && c_right<=m_right;
 action := c_left'=c_left+c_boat, m_left'=m_left+m_boat, c_boat'=0, m_boat'=0;
};

}strategy compute_post_canibals_v2 {

setMaxState(0);

setMaxAcc(200);

/*Set the initial Region*/

/*case with 5 missionaries and 4 canibals and 3 people on the boat*/

/*Region init1 := {state= boat_on_left_side && c_left=4 && m_left=5 && c_boat=0 && m_boat=0 && c_right=0 && m_right=0 && max_in_boat=3};
*/

/*general case*/

Region init2 := {state= boat_on_left_side && c_boat=0 && m_boat=0 && c_right=0 && m_right=0};

/*
Set the set of transitions used
*/
Transitions t := {EndOfTravel, LoadCanibalsOnLeftSide, LoadMissionariesOnLeftSide, CrossAndUnloadToRightSide, LoadCanibalsOnRightSide, LoadMissionariesOnRightSide, CrossAndUnloadToLeftSide};

Region good := {state=finished};
/*
Region reach1 := post*(init1, t);

if (!isEmpty(reach1 && good)) then
print("ok for the finite case");
else
print("ouch for the finite case!");
endif 
*/

Region reach2 := post*(init2, t,4);
if (!isEmpty(reach2 && good)) then
print("ok for the infinite case");
else
print("ouch for the infinite case!");
endif 


}
