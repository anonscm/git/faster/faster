#!/bin/sh 

MEMORY_USAGE_LIMIT=`expr 9 \* 512 \* 1024`
TIMEOUT_IN_MINUTES=5
TIMEOUT=`expr ${TIMEOUT_IN_MINUTES} '*' 60`
BENCHMARKS="benchmarks.lst"
SEEDS=`cat seeds.txt`
SOLVERS=`distiller -q --solvers`
BENCHFILES=`cat ${BENCHMARKS}` 

cat > GNUmakefile <<EOF
##
## This makefile has been generated automatically by $0
## 
BENCHFILES=`echo ${BENCHFILES}`

EOF

(echo "RESULTFILES= \\"
    for B in ${BENCHFILES}; do
	bench=`basename ${B} .fst`
	for G in ${SEEDS}; do
	    for S in ${SOLVERS}; do
		res="fst-no-batof/${bench}-${G}-${S}.result"
		echo "  $res \\"
	    done
	    res="fst/${bench}-${G}.result"
	    echo "  $res \\"
	done
    done
    echo "  ${nop}") >> GNUmakefile

cat >> GNUmakefile <<EOF
all : std-results.csv batof-results.csv 

std-results.csv batof-results.csv : \${RESULTFILES}
	./do-benchmark-report.sh

clean :
	rm -f \${RESULTFILES}
	rm -f \${RESULTFILES:%=%.err}
	rm -f \${RESULTFILES:%=%.time}

GNUmakefile : ${BENCHMARKS} do-benchmark-makefile.sh
	@ echo "Regenerating GNUmakefile"
	@ rm -f GNUmakefile
	@ ./do-benchmark-makefile.sh

EOF
    
for G in ${SEEDS}; do
    for S in ${SOLVERS}; do
	cat >> GNUmakefile <<EOF
fst-no-batof/%-${G}-${S}.result : fst-no-batof/%.fst GNUmakefile
	@ echo "\$@"
	@ (ulimit -v ${MEMORY_USAGE_LIMIT} -t ${TIMEOUT} ; time -p fast --seed=${G} --plugin=${S} \$< > \$@ 2> \$@.err) 2> \$@.time || echo "error" > \$@

EOF
    done

    cat >> GNUmakefile <<EOF
fst/%-${G}.result : fst/%.fst GNUmakefile
	@ echo "\$@"
	@ (ulimit  -v ${MEMORY_USAGE_LIMIT} -t ${TIMEOUT} ; time -p fast --seed=${G} --plugin=prestaf \$< > \$@ 2> \$@.err) 2> \$@.time || echo "error" > \$@

EOF
done

exit 0
