#!/bin/sh

INSTALLER="install-faster.sh"
FTPHOST="altarica.labri.fr"
FTPDIR="pub/tools/tmp"

ftp_download_package() {
    pkg="$1"
    oname="$2"
    ftp -n <<EOF
open ${FTPHOST}
user anonymous anonymous
binary
cd ${FTPDIR}
get ${pkg} ${oname}
quit
EOF
}

wget_download_package() {
    pkg="$1"
    oname="$2"
    url="ftp://${FTPHOST}/${FTPDIR}/${pkg}"
    if test "x${oname}" = "x" ; then
	wget -q ${url}
    else
	wget -q ${url} -O ${oname}
    fi
}

check_download_proc() {
   wget=""
   ftp=""
   for p in /bin /usr/bin /usr/local/bin /usr/pkg/bin /opt/bin ; do
       if test -z "${wget}" -a -x ${p}/wget ; then
	   wget="${p}/wget"
       fi

       if test -z "${ftp}" -a -x ${p}/ftp ; then
	   ftp="${p}/ftp"
       fi
   done

   proc="none"
   if test "${proc}" = "none" -a -n "${wget}" -a -x "${wget}" ; then
       proc="wget_download_package"        
   fi

   if test "${proc}" = "none" -a -n "${ftp}" -a -x "${ftp}" ; then
       proc="ftp_download_package"
   fi

   echo "${proc}"
}

build_package() {
    pkg="$1"
    pkglog="$2"
    result=0

    cd ${p}
    test -d build || mkdir build
    cd build
    if ../configure --prefix=${installdir} > ${pkglog} 2>&1 ; then
	if (make && make install) >> ${pkglog} 2>&1 ; then
	    :
	else
	    result=1
	fi
    else
	result=1
    fi
    
    return ${result}
}

if test "x$1" = "x"; then
    echo "no installation directory is specified" >&2 
    exit 1
fi

case "$1" in
    [\\/]* ) installdir="$1" ;;
    *) 
	echo "the installation directory is not an absolute path." >&2 
	exit 1
	;;
esac

download="`check_download_proc`"
if test "${download}" = "none" ; then
    echo "can't find ftp or wget command." >&2 
    exit 1
fi

topdir=`pwd`
topsrcdir=${topdir}/_sources

DOWNLOAD_LOG="${topdir}/download.log"
CPPFLAGS="-I${installdir}/include -I/usr/pkg/include"
LDFLAGS="-L${installdir}/lib -L/usr/pkg/lib"
PATH=${installdir}/bin:$PATH
export CPPFLAGS LDFLAGS PATH

rm -f ${DOWNLOAD_LOG}

echo "checking for installer changes"
NEWINSTALLER="new-${INSTALLER}"
if test -f ${NEWINSTALLER} ; then
    let N=1
    while test -f new-${INSTALLER}-${N} ; do
	N=`expr ${N} + 1`
    done
    NEWINSTALLER="new-${INSTALLER}-${N}"
fi

${download} ${INSTALLER} ${NEWINSTALLER} >> ${DOWNLOAD_LOG} 2>&1  

if cmp "$0" ${NEWINSTALLER} > /dev/null 2>&1 ; then
    echo "   installer does not require to be updated"
    rm -f ${NEWINSTALLER}
else
    chmod +x ${NEWINSTALLER}
    echo "   installer has changed. i had downloaded it into ${NEWINSTALLER}"
    exit 1
fi

test -d ${topdir} || mkdir ${topdir}
test -d ${topsrcdir} || mkdir ${topsrcdir}

cd ${topsrcdir}
packages="genepi-current fast-current omega-plugin-1.0 ccl-current sataf-current armoise-current alambic-current prestaf-current"


echo "downloading packages (progress shown in ${DOWNLOAD_LOG})"
for p in ${packages}; do
    if test -f ${p}.tar.gz ; then
	echo "   ${p} is already here" 
    else
	echo "   downloading ${p}"
	${download} ${p}.tar.gz >> ${DOWNLOAD_LOG} 2>&1  
    fi
done

echo "installing packages"
for p in ${packages} ; do
    if test -f ${p}.tar.gz ; then
	gzip -dc ${p}.tar.gz | tar xf -    
	echo "   installing ${p} (progress shown in ${topdir}/${p}.log)"
	if (build_package ${p} ${topdir}/${p}.log) ; then
	    :
	else
	    echo "    error: an error occurs while installing ${p}. have a look to ${p}.log" >&2
	    exit 1
	fi
    else
	echo "can't find downloaded packages ${p}" >&2
	exit 1
    fi
done

exit 0
