#!/bin/sh

rm -fr libltdl
TOLOWER="tr '[A-Z]' '[a-z]'"
mycpu=`(uname -p 2>/dev/null || uname -m 2>/dev/null || echo unknown) | \
        ${TOLOWER}`
myos=`uname -s | ${TOLOWER}`

case "$mycpu" in
    unknown|*" "*)
        mycpu=`uname -m | ${TOLOWER}` ;;
esac

LIBTOOLIZE=libtoolize

case ${myos} in
    darwin*)
	$(which -s glibtoolize) && LIBTOOLIZE=glibtoolize
	;;
esac

LIBTOOLIZE="${LIBTOOLIZE} --force -i" autoreconf -f -i . 

