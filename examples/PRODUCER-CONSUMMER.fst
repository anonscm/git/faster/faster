/**
 * FAST, an accelerated symbolic model-checker. 
 * Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, 
 * Alain Finkel (coordinator) and LSV, CNRS UMR 8643 & ENS Cachan.
 *
 * FAST is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation; either version 2, or (at your option) any later version.
 *
 * FAST  is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FAST; see the file COPYING.  If not, write to the Free Software Foundation, 
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

model PRODUCER_CONSUMMER {

var   i,b,a,o1,o2;

states normal;

transition r1 := {
from := normal;
to := normal;
guard := i>=1;
action :=	b'=b+1, i'=i-1;
};
  	      
transition r2 := {
from := normal;
to := normal;
guard := b>=1;
action := o1'=o1+1, b'=b-1;
};

transition r3 := {
from := normal;
to := normal;
guard := b>=1;
action := o2'=o2+1, b'=b-1;
};

}



strategy s1 {

setMaxState(0);
setMaxAcc(100);

Region init := {o1=0 && state=normal && b=0 && o2=0 && a>=0 && a-i=0};
Transitions t := {r1,r2,r3};

Region reach := post*(init, t);
print (reach);	
}
