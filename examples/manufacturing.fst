/**
 * FAST, an accelerated symbolic model-checker. 
 * Copyright (C) 2003 Jerome Leroux, Sebastien Bardin, 
 * Alain Finkel (coordinator) and LSV, CNRS UMR 8643 & ENS Cachan.
 *
 * FAST is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation; either version 2, or (at your option) any later version.
 *
 * FAST  is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FAST; see the file COPYING.  If not, write to the Free Software Foundation, 
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */
model manufacturing {




var    p1, p2, p3, p4, p5, p6, p7;
//, t1, t2, t3, t4, t5, t6
// les ti comptent le nombre de fois que la transition est prise


states normal;

transition t1 := {
	from:=normal;
	to:=normal;
	guard :=
	  p1 >= 2;
	action :=
	  p1'=p1-2,
	  p5'=p5+2;
	//  ,t1'=t1 + 1
};	

transition t2 :={
 from:= normal;
to:= normal;
	guard:=
	  p2 >=1 ;
	action:=
	  p2'=p2 - 1,
	  p5'=p5+1;
	 // ,t2'=t2+1
};
	
transition t3 :={ 
	from:= normal;
	to:=normal;
	guard:=
	  p3 >= 1 ;
	action:=
	  p3'=p3-1,
	  p6'=p6+1;
	 // ,t3'=t3+1
};	

transition t4 :={ 
	from:=normal;
	to:=normal;
	guard:= 
	  p5>=4 && p6>=1;
	action:=
	  p5'=p5-4,
	  p7'=p7+1,
	  p6'=p6-1;
	 // ,t4'=t4+1
};	

transition t5 :={ 
	from:= normal;
	to:=normal;
	guard:=
	  p4>=1 ;
	action:=
	  p4'=p4-1,
	  p7'=p7+1;
	 // ,t5'=t5+1
};	

transition t6 :={ 
	from:=normal;
	to:=normal;
	guard:=
	  p7 >=2 ;
	action:=
	  p7'=p7-2,
	  p1'=p1+3,
	  p2'=p2+1,
	  p3'=p3+1,
	  p4'=p4+1
	 //, t6'=t6+1
	;
};
}

strategy s1 {

setMaxState(0);
setMaxAcc(100);

Region init := {state=normal &&   p5 = 0 && p6 = 0 && p7 = 0 /* && t1 = 0 && t2 = 0 && t3 = 0 && t4 = 0 && t5 = 0 && t6 = 0 */ };
Transitions t := {t1,t2,t3,t4,t5,t6};

Region reach := post*(init, t);
print (reach);
}
